
# Beiträge zum Source Code

Vielen Dank, dass du etwas zur Weiterentwicklung von openDesk beitragen möchtest. Darüber freuen wir uns sehr!

Bevor wir deine Beiträge übernehmen können, benötigen wir von dir eine unterschriebene Version eines Contributor Licence Agreements (CLA), das du hier findest: [CLA_de.pdf](https://gitlab.opencode.de/bmi/opendesk/info/-/blob/main/CLA/CLA_de.pdf).

Fülle das CLA aus und schicke uns das unterschriebene Exemplar per E-Mail an ***opendesk@zendis.de***.

Sobald wir das Contributor Licence Agreement von dir erhalten haben, werden wir deine Beiträge übernehmen können.

# Code Contributions

We are happy that you are looking into contributing code to openDesk.

Before we can accept your contributions, we need a signed version of a Contributor License Agreement (CLA) from you. Please download the template from here: [CLA_en.pdf](https://gitlab.opencode.de/bmi/opendesk/info/-/blob/main/CLA/CLA_en.pdf). Fill out the CLA and email the signed copy back to us.

Please send the signed CLA to the following email address: ***opendesk@zendis.de***.

As soon as we have received the Contributor License Agreement from you, we will be able to accept your contributions.
