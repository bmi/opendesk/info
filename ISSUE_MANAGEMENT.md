<h1>Issue management on Open CoDE</h1>

<!-- TOC -->
* [Disclaimer](#disclaimer)
* [Language](#language)
* [GitLab basics](#gitlab-basics)
  * [Group / Project hierarchy](#group--project-hierarchy)
  * [Issues](#issues)
  * [Labels](#labels)
  * [Issue Boards](#issue-boards)
* [openDesk](#opendesk)
  * [Projects for issues](#projects-for-issues)
  * [Labels](#labels-1)
    * [Category labels](#category-labels)
      * [Status (mutually exclusive)](#status-mutually-exclusive)
      * [InCharge (mutually exclusive)](#incharge-mutually-exclusive)
      * [Prio (mutually exclusive)](#prio-mutually-exclusive)
      * [Source (mutually exclusive)](#source-mutually-exclusive)
      * [Type (mutually exclusive)](#type-mutually-exclusive)
      * [Owner](#owner)
    * [Atomic labels](#atomic-labels)
<!-- TOC -->

# Disclaimer

This document provides a basic understanding of the GitLab hierarchy and how openDesk manages their issues within the [info repository](https://gitlab.opencode.de/bmi/opendesk/info). If you just want to provide feedback you do not need to read this document in full detail, you can go ahead and [create a new issue](https://gitlab.opencode.de/bmi/opendesk/info/-/issues/new) explaining your finding and the desired behavior.

# Language

On the operational level, English is the preferred language. Of course, we will also handle issues written in German language and for consistency within a single issue, we strive to stay with the initial language of an issue.

# GitLab basics

Some basics that are good to know when it comes to working with issues in GitLab.

## Group / Project hierarchy

Groups are used to define a hierarchical structure that should be tailored to the needs of a specific scenario, in our case it is making openDesk publicly available, providing transparency to the community, and getting the community involved.

While groups can consist of subgroups and projects, the projects are the ones that contain the real content, e.g. issues, source code, and documentation.

Projects are the leaves of the hierarchical structure, so there are no such things as subprojects or subgroups on a project.

Find the openDesk base group here: https://gitlab.opencode.de/groups/bmi/opendesk

For further reading: https://docs.gitlab.com/ee/user/group/subgroups/

## Issues

Issues created within a project are visible to the upwards hierarchy (parents). So looking at the issue list within the GitLabs root group shows you all issues of the GitLab (you have access to).

Look at the issue list of the openDesk base group: https://gitlab.opencode.de/groups/bmi/opendesk/-/issues

For further reading: https://docs.gitlab.com/ee/user/project/issues/

## Labels

Labels are important to categorize issues to keep an overview especially when dealing with a large amount of issues. Labels are visible to the downwards hierarchy (children). So creating a label in the GitLab root group makes it available for all GitLab groups and projects, updating therefore affects all child entities.

Sometimes we need to make use of specific labels tailored to the needs of a subgroup or project. In that case, it is recommended to synchronize with others and to define labels as close to the base group as possible or ideally within the base group itself. This is especially important if you want to make use of higher-level boards to consolidate your view on the issues within the below hierarchy.

Check the openDesk base groups label view: https://gitlab.opencode.de/groups/bmi/opendesk/-/labels

For further reading: https://docs.gitlab.com/ee/user/project/labels.html

## Issue Boards

Boards are the central tool to manage issues. Like labels boards can be created on group or project level. When no filter is active, a board shows all the issues from the downwards hierarchy.

With the GitLab Free Tier that is operated on Open CoDE, we have limits regarding the management of issues with boards especially:
- Only a single board can be defined on a group.
- Board configuration (columns) is only possible based on labels.

For further reading: https://docs.gitlab.com/ee/user/project/issue_board.html

# openDesk

## Projects for issues

We expect a significant number of issues to deal with so we need proper organization using labels and boards.

We collect issues in two different projects:
- [Enduser feedback](https://gitlab.opencode.de/bmi/opendesk/nutzerfeedback/-/issues): Covers scenarios where somebody is using the webinterface of openDesk or clients interacting with the openDesk applications.
- [Technical feedback](https://gitlab.opencode.de/bmi/opendesk/deployment/opendesk/-/boards): Addressing the component deployment and operation.

## Labels

We will have a couple of label categories described in this section. A category can be understood as a group of labels belonging to the same topic.

Using the GitLab Free Tier we cannot use so-called [scoped labels](https://docs.gitlab.com/ee/user/project/labels.html#scoped-labels) that enforce mutual exclusive usage of labels from a single category. So it is recommended to manage these category labels on a board where the columns are the labels of a given category, so moving an issue between the columns automatically removes the old column label and sets the new column one.

We will also have atomic labels to tag issues with certain information.

### Category labels

This section explains the labels (sets) or categories we make use of. All categories have their own set of labels and a link to the project board that shows all issues grouped by the given category.

#### Status (mutually exclusive)

To have a simple workflow for our issues we make use of the status labels below, while `open` and `closed` are default status columns that are shown in a freshly created issue board. The status labels define the flow of the issues that should be understood as "top-down" (or left-right on an issue board):

- `open`: Newly created issues that are untouched yet regarding their status.
- `status: New`: Issues that have been registered by the team but are not yet detailed enough to get into the next status.
- `status: Ready`: Issues that have a clear scope and description of what has to be achieved to close the issue. Also, they have to be labeled in the following categories: `incharge`, `party`, `prio`, `source`, and `type`.
- `status: Progress`: The issue is being worked on by the assignee (or colleagues of the assignee).
- `status: Test`: The issue can be tested, it's implementation has to be verified.
- `closed`: When the test is successful the issue can be closed.

Of course, issues can also be moved "backwards", e.g. if a test fails an issue should be moved to `ready` again.

**Status-Board**: https://gitlab.opencode.de/bmi/opendesk/info/-/boards/264

#### InCharge (mutually exclusive)

With this label category, we make transparent who is in charge of resolving this issue.

- `incharge: Platform`: Issue can be implemented in the openDesk platform.
- `incharge: Project`: This issue needs to be specified or implemented by the project, most likely the openDesk product management has to follow up on this one.
- `incharge: Upstream`: Has to be implemented in the upstream product(s) so it is the vendor(s) responsibility.

**InCharge-Board**: https://gitlab.opencode.de/bmi/opendesk/info/-/boards/1188

#### Prio (mutually exclusive)

Priorities are always important but also always debatable. We want to keep it simple by using the following four priority levels:

- `prio: Urgent`: An urgent issue that impacts all users making them unable to work with one or more of the functional components and/or addressing a severe security risk.
- `prio: High`: A high impact on the usability of one for more functional components affecting a large percentage of the potential users or an issue heavily impacting the operation of an openDesk installation, especially from a security perspective.
- `prio: Medium`: An issue users or operators of openDesk will see as problematic, but there might be a workaround or there is no impact on the functional usage or operations of openDesk.
- `prio: Low`: Something that would improve usage or operation of openDesk.

**Prio-Board**: https://gitlab.opencode.de/bmi/opendesk/info/-/boards/1185

#### Source (mutually exclusive)

As there are a lot of different people looking at openDesk we get feedback out of quite some channels. To understand where the issue originally came from we use the "source" label:

- `source: Community`: Issues identified by the community
- `source: Operations`: Operational issues that occurred within the project context
- `source: Product management`: Topic identified by product management
- `source: QA`: Project internal Quality Assurance
- `source: Usability research`: From the project's usability research activities.

**Source-Board**: https://gitlab.opencode.de/bmi/opendesk/info/-/boards/1187

#### Type (mutually exclusive)

To understand what kind of issue it is we will label them according to their type:

- `type: Bug`: A broken feature or something that is not intended to work this way.
- `type: Improvement`: A new feature or usability improvement.
- `type: Idea`: If a not overly specified issue or a strategic development aspect comes up we track it as an "idea" that is most likely handled by product and/or requirements management first and might end up in a followup ticket of type "improvement".

**Type-Board**: https://gitlab.opencode.de/bmi/opendesk/info/-/boards/1184

#### Owner

The owner(s) of the component(s) affected by the issue.

- `owner: ALL`: If it is a cross-functional issue affecting all components.
- `owner: Collabora`
- `owner: CryptPad`
- `owner: Element`
- `owner: Nextcloud`
- `owner: Nordeck`
- `owner: Open-Xchange`
- `owner: OpenProject`
- `owner: Univention`
- `owner: XWiki`

### Atomic labels

Some of the atomic labels we use:

- `feedback needed`: When the the assignee needs feedback to advance the issue.
- `underspecified`: When there is not enough information on that issue for the party in charge to work on that issue.
