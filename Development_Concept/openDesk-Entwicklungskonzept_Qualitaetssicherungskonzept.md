---
pdf_footer: "openDesk Qualitätssicherungskonzept"
---

## Qualitätssicherungskonzept

## Grundsätzliches

In diesem Dokument wird das Qualitätssicherheitskonzept für openDesk beschrieben. Bei openDesk handelt es sich um eine Software, die aus Komponenten mehrerer Hersteller zusammen gestellt wird. Jede Komponente der beteiligten Hersteller unterliegt für sich bereits einem Qualitätssicherungskonzept, mit dem die Qualität des jeweiligen Softwareproduktes sicher gestellt wird. 
Dieses übergreifende Konzept zielt dabei auf die gemeinsame Funktionsweise des Gesamtprodukts mit dem Fokus auf das Interagieren der einzelnen Bestandteile miteinander, um so auch diesen Aspekt auf ein höchstmögliches Qualitätsniveau heben zu können.

Die Komponenten von openDesk können in der Zukunft auch austauschbar und erweiterbar sein. Das hier dargestellte Qualitätssicherheitskonzept berücksichtigt dies und bindet die Qualitätssicherung der beteiligten Hersteller modular ein. Jeder beteiligte Hersteller an openDesk ist verpflichtet dieses Qualitätssicherungskonzept zu unterstützen. Bei der Auswahl zukünftiger Hersteller sollte dies als Kriterium bei der Auswahl berücksichtigt werden, damit die Gesamtqualität von openDesk gewährleistet bleibt.

Im Abschnitt "Umfang der Qualitätssicherung" wird die Unterscheidung dieser aufeinander aufbauenden Qualitätssicherungen näher erläutert.

Für die vom ZenDiS beauftragten Anforderungen werden Testfälle durch das QS-Team des Generalunternehmers erstellt und mit den Herstellern abgestimmt.

Die Testabdeckung wird über dokumentierte Testfälle festgelegt. Details zur Testabdeckung werden ebenfalls im Abschnitt "Umfang der Qualitätssicherung" beschrieben.

Im Abschnitt "Umfang der Qualitätssicherung" wird darüber hinaus festgelegt, welche Testtypen in der Qualitätssicherung von openDesk angewendet werden.

Testergebnisse im Rahmen der Qualitätssicherung von openDesk sind grundsätzlich schriftlich zu dokumentieren. Diese schriftliche Dokumentation ist Basis für die jeweilige Freigabe eines Releases. Die Details der Dokumentation werden im Abschnitt "Dokumentation von Testergebnissen" beschrieben.

Die Hersteller der bereitgestellten Software Module streben innerhalb ihres Qualitätssicherheitsprozesses eine größtmögliche Abdeckung mit automatisierten Tests an. Das QS-Team der Generalunternehmers stellt sicher, dass modulübergreifende Tests abgedeckt sind, wenn diese im Rahmen der Beauftragung notwendig sind.

Im Abschnitt "Fehlerkategorien" wird definiert, welche Fehlerkategorien es bei openDesk gibt. Diese Fehlerkategorien legen die Basis für die darauf aufbauende Durchführung von Tests.

Der Ablauf der Qualitätssicherung ist im Abschnitt "Durchführung der Tests" näher erläutert.

Der Abschnitt "Voraussetzungen für das Testen" beschreibt abschließend die Bedingungen, die von den beteiligten Parteien erfüllt sein müssen, damit dieses Testkonzept erfüllt werden kann.

## Umfang der Qualitätssicherung

Die Qualitätssicherung der Herstellermodule erfolgt in der Verantwortung der jeweiligen Hersteller, im Rahmen der Qualitätssicherung der Upstream-Entwicklung des jeweiligen Moduls.
Speziell für openDesk beinhaltet diese Qualitätssicherung explizit auch die Funktionen, die für openDesk vereinbart und entwickelt werden. openDesk Implementierungen unterliegen damit den selben strengen Qualitätskriterien der jeweiligen Herstellermodule. Abweichungen von diesen Qualitätskriterien - wie etwa das Bereitstellen eines Pre-Releases für openDesk - bedürfen der beiderseitigen Vereinbarung zwischen ZenDiS und dem jeweiligen Hersteller.

Für den Fall, dass sich openDesk Funktionen bei der Implementierung auf mehrere Herstellermodule auswirken, verpflichten sich die Hersteller mit der Vereinbarung zur Umsetzung der Funktion, dass sie in den betroffenen Module auch für diese modulübergreifenden Funktionen die Qualität sicherstellen.

Auf diese Weise qualitätsgesicherte Module werden durch die Hersteller in der Integrationsumgebung für openDesk bereit gestellt. Die Hersteller stellen sicher, dass ihre Module in der Integrationsumgebung lauffähig und funktional sind.

Ab diesem Punkt beginnt die Verantwortung des QS-Team (Generalunternehmer). Das QS-Team (Generalunternehmer) 
prüft hier die im Projekt vereinbarten Funktionen und führt die Abnahme auf einer dafür geeigneten Testumgebung durch. Der Abnahmetest prüft den Funktionsumfang auf Basis der zuvor vereinbarten Anforderungen.

Die folgenden Abschnitte beziehen sich ausschließlich auf den hier beschriebenen Abnahmeprozess durch das Product Owner Team (ZenDiS).

Der Abnahmetest umfasst folgende Testarten:

* Funktionaler Test
* Usability Test

Die funktionalen Tests stellen sicher, dass die vereinbarten Anforderungen in der ausgelieferten Software erfüllt sind, und die Funktionalität vollständig einsetzbar ist.
Die Usability Tests stellen sicher, dass die Software den vereinbarten Anforderungen an Usability, wie z.B. übergreifende openDesk Navigation, Anforderungen an Barrierefreiheit, Nutzbarkeit des Nutzerinterfaces gegeben sind.

Last- und Performance-Tests werden unabhängig vom Abnahmetest in dem dafür vorgesehenen Leistungsbaustein W.1.4 vereinbart und durchgeführt.

Sicherheitstests werden unabhängig vom Abnahmetest in dem dafür vorgesehenen Leistungsbaustein W.5.3 vereinbart und durchgeführt.

## Dokumentation von Testergebnissen

Zur Nachvollziehbarkeit werden Testergebnisse in einem dafür vereinbarten Prozess dokumentiert. 
Die Dokumentation der Testergebnisse muss Folgendes beinhalten:

* Ausführliche Beschreibung des Fehlverhaltens
* Schrittweises Vorgehen des Test zur Reproduktion des Fehlverhaltens
* Angabe der Umgebung, in der das Fehlverhalten beobachtet wurde
* Angabe der Version der getesteten Software
* Datum und Uhrzeit des Tests
* Angabe des Nutzerzugangs, mit dem der Test durchgeführt worden ist.
* Version des verwendeten Web-Browsers sowie Betriebssystem 
* Referenz zur Anforderung, die nicht erfüllt worden ist.
* Eine Einschätzung der Schwere der Funktionsabweichung im Sinne der im Folgenden Abschnitt definierten Fehlerkategorien.

Das QS-Team erstellt für die gefundenen Fehler Fehlerberichte. Sofern der Fehler einem Modul zugeordnet werden kann, erfolgt der Fehlerbericht gemäß der Contribution Richtlinien der jeweiligen Upstream Projekte (siehe Abschnitt "Zusammenarbeit mit Herstellern").

Ist eine solche Zuordnung durch das QS-Team nicht möglich, wird der Fehlerbericht im entsprechenden Repository auf Open CoDE erfasst.

> [!IMPORTANT]
>
> Bei der Erstellung von Fehlerberichten ist die Erstellung von Duplikaten zu vermeiden.

## Fehlerkategorien

Für die Bewertung der Auswirkung von Fehlern werden Fehlerkategorien vereinbart. Anhand der Fehlerkategorien können die beteiligten Parteien ein einheitliches, abgestimmtes Vorgehen festlegen, mit welcher Priorität die Behebung von Fehlern erfolgen soll.
Die Fehlerkategorien, werden insbesondere im Abnahmeprozess herangezogen.

**Blocker**

Ein Fehler der Kategorie *Blocker* ist so schwerwiegend, dass eine Kernfunktion von openDesk nicht nutzbar ist, oder eine große Zahl von Nutzenden in der Bedienung der Software stark eingeschränkt ist. Fehler dieser Kategorie sind releaseverhindernd. Der Auftragnehmer ist verpflichtet schnellstmöglich eine Behebung für den Fehler bereit zu stellen. 

**Schwerer Fehler**

Ein Fehler der Kategorie *schwerer Fehler* ist eine deutliche Einschränkung der Nutzbarkeit der Software. Die Software kann aber weiter genutzt werden oder der Fehler durch einen akzeptablen Workaround umgangen werden. Fehler dieser Kategorie sind nicht releaseverhindernd. Der Auftragnehmer ist jedoch verpflichtet schnellstmöglich ein Patch-Release für die Behebung bereit zustellen.

**Kleiner Fehler**

Ein Fehler der Kategorie *kleiner Fehler* ist ein Abweichung der Software von der vereinbarten Anforderung, der die Nutzung kaum oder nur in besonderen Situationen beeinträchtigt. 
Fehlerbehebungen dieser Kategorie werden im Rahmen des üblichen Releaseprozesses, mit einem zukünftigem Release behoben.

## Testdurchführung

Der Abnahmetest erfolgt umgehend nach Bereitstellung der Software auf dem dafür vereinbarten Testsystem. Möglicherweise entdeckte Fehler werden dem Auftragnehmer und dem betroffenen Hersteller umgehend nach Abschluss des Testlaufs zur Kenntnis gegeben. Fehlerbeschreibungen werden explizit nicht bis zur vollständigen Durchführung der Tests zurückgehalten. Auf diese Weise wird dem Auftragnehmer die Möglichkeit gegeben, eine Fehlerkorrektur schnellstmöglich bereit zustellen.

Der Auftragnehmer hat die Möglichkeit den Auftraggeber im Vorwege des Abnahmetests um einen Vorabtest zu bitten. Dies sollte insbesondere dann erfolgen, wenn weitreichende Veränderungen der Software in einer Lieferung erwartet werden. Auf diese Weise soll eine mögliche Abweichung von Anforderungen frühzeitig bemerkt werden. Der Zeitraum für mögliche Nachbesserungen wird auf diese Weise erweitert. (Fail-Fast-Prinzip)
Die Testumgebung für solche Vorabtests wird durch den Generalunternehmer bzw. den Hersteller der Software bereit gestellt.

Die Ergebnisse des Abnahmetests und insbesondere die Bewertung der Fehler sind die Ausgangsbasis für die Abnahme der Software im Sinne der zuvor vereinbarten Anforderung. Die Abnahmeerklärung beinhaltet dabei sowohl die Abnahme im Sinne der Projektvereinbarung, als auch die Auswirkung auf die Releasebereitstellung. 

Die Inbetriebnahme einer Funktion ist grundsätzlich die implizite Abnahme der bereitgestellten Funktionalität. Eine Abnahme kann auch unter dem Vorbehalt der Nachbesserung dokumentierter Fehler erfolgen. In diesem Fall ist die Inbetriebnahme keine implizite Abnahme der bereitgestellten Funktionalität.

## Voraussetzungen für das Testen

Für die erfolgreiche Durchführung der Abnahmetests sind folgende Voraussetzngen zu erfüllen:
* Eine geeignete Testumgebung steht zur Verfügung.
* Es wird die von den Herstellern bereitgestellte Software in der dafür vorgesehenen Version eingesetzt.
* Die Hersteller haben im Vorwege ihre Software gemäß ihrer Qualitätsvorgaben für ihre Software durchlaufen. Der Hersteller garantiert mit der Auslieferung, dass die Software die Qualitätskriterien des Herstellers erfolgreich bestanden hat.
* Der Abnahmetest basiert auf den gemeinsam vereinbarten Anforderungen. Diese Anforderungen sind zwischen der ZeDiS, dem Generalunternehmer und den  beteiligten Herstellern im Rahmen der Einzelbeauftragungen vor Implementierungsbeginn zu vereinbaren.
* Der Abnahmetest deckt die vereinbarten Anforderungen der zu testende Auslieferung vollumfänglich ab.
* Das eingesetzte Personal für die Testdurchführung besitzt ausreichend Erfahrung in der openDesk Funktionalität, dem vereinbarten Testprozess für openDesk und Qualitätssicherung von Software im allgemeinen.















