---
pdf_footer: "Zusammenfassung openDesk Entwicklungskonzept"
---

# Zusammenfassung: Konzept für die Zusammenarbeit bei der Weiterentwicklung

Dieses Dokument ist eine Zusammenfassung der Ausschreibungsunterlagen der Ausschreibung "*Rahmenvertrag über Weiterentwicklungs- und Supportleistungen für openDesk*". 

Diese Struktur orientiert sich an den in der Ausschreibung definierten Bewertungskritieren.

* [Konzept für die Projektsteuerung und den Personaleinsatz](./openDesk-Entwicklungskonzept_Konzept-Projektsteuerung-und-Personaleinsatz.md)
* [Mind. drei Ideen und Konzepte zur Weiterentwicklung](./openDesk-Entwicklungskonzept_Ideen-und-Konzepte-Weiterentwicklung.md)
* [Zusammenarbeit mit den Herstellern](./openDesk-Entwicklungskonzept_Zusammenarbeit-mit-Herstellern.md)
* [Qualitätssicherungskonzept](./openDesk-Entwicklungskonzept_Qualitaetssicherungskonzept.md)

## Executive Summary

1. This project team will jointly develop openDesk with ZenDiS as a flagship project for Europe's digital sovereignty.

2. The General Contractor (GC: B1 Systems) is bringing together the innovative power and development efforts of the involved open-source providers to create the world's best open-source communication and collaboration platform for public administration.

3. Through upstream contributions, ZenDiS benefits from the developments and innovations of a global open-source community. B1 Systems and the participating open-source providers are unlocking this potential to ensure the sustainable and future-proof development of openDesk, based on the requirements defined by ZenDiS in its role as Product Owner of openDesk.

## Memorandum of understanding: Key aspects of the collaboration

1. Public money - Public Code. All roadmap sponsorhip by ZenDiS will be part of the Vendor's community editions. The Vendors are free to further develop enterprise features which are not released through their community editions.
2. A key aspect of the openDesk project is to integrate the different modules for a superior user experience.
3. The vendor's core business model is to provide enterprise subscriptions instead of providing development services (projects).
4. General contractor provides upstream contributions to the main projects. If at all possible, the General Contractor does not support ZenDiS to fork the existing upstream projects and communities.
5. The Vendors have to assure the quality of their releases like they already do as part of their SLA. This also includes the integration between modules which are not openDesk-specific. For openDesk-specific integrations the General Contractor does the integration testing. Here we strive to find synergies with upstream contributions to the test suite (open source).
6. We strive for an international development community. The default project language in the project teams is English. ZenDiS can decide to have contracts and external communication and user documentation to be German (Amtssprache).
7. ZenDiS leads the Product Owner Team of the openDesk product. The Vendors remain Product Owners of their products.
8. The Vendors as the resource owners of the development capacities are members of the project steering committee (openDesk Community Board).
9. We have continuous and regular releases of openDesk. Development of the different openDesk modules need to be independently releasable. Delays in one Vendor’s release must not block the openDesk releases.
10. Decisions about the internal delivery processes remain with the Vendors. 
11. ZenDiS is the final decision making authority for all openDesk projects. The Vendors decide about the roadmaps of their products. It is our goal to align the interests of all parties.
12. We use openDesk and openCode for collaboration and management of the openDesk projects.
13. We allow only professionals with the required skills and experience to work on the projects.
14. We implement rolling forecasts and planning processes to level-out the required resources with the available capacities. The responsible parties avoid delays, peaks and interruptions on all planning levels. The GC and the Vendors are responsible, ZenDiS as project owner is accountable.
15. ZenDiS decides on the delivery model (fixed price vs. agile).
