---
pdf_footer: "openDesk Zusammenarbeit mit Herstellern"
---

## Konzept für die Zusammenarbeit mit den Herstellern

Bei openDesk handelt es sich um eine Kombination von mehreren am Markt existierenden Produkten. Dadurch kann openDesk auf langjährige Erfahrung der Entwicklungsteams und ausgereifte Produkte zurückgreifen. Gleichzeitig bedeutet es aber auch, dass in der Zusammenarbeit ein diesem Umstand rechnungstragendes Vorgehen notwendig ist, um mit allen Herstellern das bestmögliche Ergebnis zu erzielen.

Im Rahmen dieses Konzeptes bietet der Auftragnehmer Entwicklungsleistungen aller Hersteller der Module von openDesk für ihre jeweiligen Produkte an. Das Ziel ist es, dass alle Entwicklungen Upstream in die Kernprodukte der jeweiligen Hersteller fließen, d.h. ZenDiS nimmt im Rahmen dieser Entwicklungsprozesse Einfluss auf die Roadmap der Hersteller und finanziert deren Entwicklung entsprechend. Daraus ergeben sich ein paar grundlegende Vorteile und Konsequenzen.

Grundlegende Vorteile des Ansatz:

1. Die Software kann im Rahmen der jeweiligen Upstream Projekte langfristig über den beauftragten Funktionsumfang hinweg weiterentwickelt werden. 
2. Tiefe Integration in alle bereits vorhandenen Funktionen, was letztlich auch zu einer höhere Zufriedenheit der Endanwender führt.
3. Eine fortlaufende Pflege der entwickelten Software und Kompatibilität mit zukünftigen Releases ist gewährleistet.
4. ZenDiS profitiert von allen bereits vorhandenen Vorteilen der jeweiligen Upstream Projekte und der Open Source Community, wie z.B. automatisierte Übersetzungen der Software, Teilnahme an den Security Programmen der jeweiligen Hersteller und eine kontinuierliche Pflege und Weiterentwicklung im Rahmen der normalen Weiterentwicklung.
5. Möglichst schnelle und agile Entwicklungsprozesse, da sie sich nahtlos in die Entwicklerteams integrieren.

Gleichzeitig bedeutet das aber auch, dass die Entwicklung in der Hand des jeweiligen Herstellers liegt. Nur so können die oben genannten Vorteile verwirklicht werden. Konkret bedeutet das:

1. Die Entwicklungen findet in den jeweiligen Repositories der Hersteller statt und fließen direkt in das jeweilige Produkt (upstream).
2. Dadurch, dass es sich um Weiterentwicklungen bestehender Produkte handelt, liegt die Ausgestaltung des Entwicklungsprozess und Releasemanagement der entsprechenden Teilkomponente (nicht für openDesk als Ganzes) beim Hersteller der jeweiligen Komponente und folgt den gleichen Richtlinien wie die allgemeine Produktentwicklung des Herstellers. Dies bedeutet auch, dass die im Weiteren beschriebenen Prozesse von Hersteller zu Hersteller leicht abweichen können.
4. Alle Community Aktivitäten wie Fehlerberichte, Beiträge aller Art (Softwarecode, Dokumentation,...) laufen über die vom Hersteller bereitgestellten Kanäle und integrieren sich so nahtlos in die bestehenden Open Source Communities der entsprechenden Software Komponente.

Unter Berücksichtigung dieser Annahmen wird im Folgenden die Zusammenarbeit mit den Herstellern im Detail beschrieben.

### Zusammenarbeit bei der Entwicklung neuer Funktionen

#### Vorschlag und Annahme von Weiterentwicklungen

Vorschläge für Softwareentwicklungen innerhalb des jeweiligen Moduls oder in openDesk insgesamt können sowohl vom Hersteller als auch von ZenDiS kommen.

Die Kommunikationskanäle werden dabei maßgeblich durch die Projektorganisation bestimmt, welche im Abschnitt "Konzept für die Projektsteuerung und den Personaleinsatz" beschrieben ist.
Es ist Aufgabe der vorschlagenden Partei, die gewünschte Funktion in einem angemessenen Detailgrad zu beschreiben. Entwicklungsvorschläge seitens ZenDiS werden durch den Auftragnehmer im Rahmen der Gesamtprojektleitung aufbereitet und an den entsprechenden Hersteller weitergegeben. Soweit möglich findet die Planung der neuen Funktion und deren Umsetzung zwischen dem Hersteller und der Gesamtprojektleitung statt. Besonders bei komplexen Neuentwicklungen kann es im Einzelfall nötig sein, dass die Abstimmung direkt zwischen ZenDiS und dem Hersteller erfolgt. Dies kann zu einer effizienten und möglichst klare Beschreibung der gewünschten Funktion beitragen. In diesem Fall wird ein solcher Dialog über die Gesamtprojektleitung organisiert. Entwicklungsvorschläge seitens der Hersteller werden von diesen der Projektleitung vorgelegt, welche die Idee wiederum mit dem ZenDiS koordiniert. Auch hier kann es Sinn machen einen direkten Kommunikationskanal zwischen Hersteller und Auftraggeber zu ermöglichen um den Vorschlag angemessen zu diskutieren. Wenn einer der Parteien einen direkten Austausch zu einer Weiterentwicklung wünscht, sollte dies ermöglicht werden.

Im Rahmen der Bewertung einer möglichen Weiterentwicklung können alle involvierten Parteien auf die genaue Ausgestaltung der neuen Funktionalität Einfluss nehmen. Nur wenn der Vorschlag am Ende sowohl in das Gesamtbild von openDesk passt, als auch zum jeweiligen Produkt und dessen Roadmap, kommt es zu einer entsprechenden Beauftragung der Softwareentwicklung.

Da es das Ziel ist, dass alle Entwicklungsleistungen Upstream in das Produkt einfließen, ergibt sich der zeitliche Ablauf der Entwicklungsleistung aus der aktuellen Releaseplanung des Herstellers.

#### Vorbereitung

Haben sich beide Parteien entschieden, eine Weiterentwicklung im Rahmen des openDesk Projekts vorzunehmen, erstellt der Hersteller eine finale Beschreibung der Entwicklungsleistung in der der zu erwartende Funktionsumfang klar definiert ist. Hierzu wird auf die Standardprozesse des jeweiligen Herstellers zur Dokumentation und Spezifikation der beauftragten Entwicklung zurückgegriffen. Nur so ist es möglich, dass sich der gesamte Prozess nahtlos und die Entwicklungsprozesse des Herstellers integriert und das bestmögliche Ergebnis erzielt wird. Dadurch wird dem Auftraggeber ein gemeinsam definiertes Endergebnis in einem gemeinsam definierten Zeitrahmen zugesichert, der Weg dahin folgt den etablierten Entwicklungsprozessen des Herstellers. Nur so ist später eine nahtlose Integration in das Kernprodukt gewährleistet. Dieses Dokument wird dem Projektmanagement zur Abnahme und finalen Beauftragung vorgelegt.

Beauftragungen von Softwareentwicklungen sind sowohl auf Stundenbasis als "Time & Material" als auch in der Form eines Festpreisangebots möglich. Die Wahl der Modalitäten kann Einfluss auf die zugesicherten Endergebnisse haben. Während bei einem Festpreisangebot eine Funktion am Ende der Entwicklung zugesichert werden kann, können unerwartete Aufwände bei einem T&M Projekt zur Reduktion des Funktionsumfangs bzw. zu Mehrkosten führen. Auf der anderen Seite kann bei einem T&M Projekt flexibler auf mögliche Änderungswünsche während der Entwicklung eingegangen werden. Unter Abwägung dieser Vor- und Nachteile wird vorab einvernehmlich zwischen Auftraggeber und Auftragnehmer eines der beiden Modelle ausgewählt.

#### Entwicklung und Veröffentlichung

Die gesamte Entwicklung geschieht in den Entwicklungsteams des Herstellers. Dies bedeutet, dass sich alle Entwicklungs-, Q&A-, Change-Management- und Release-Prozesse sowie die Dokumentation an den etablierten Prozessen des jeweiligen Herstellers orientieren. Nur so sind upstream Beiträge in die entsprechenden Kernprodukte möglich. 

Abstimmungsbedarf der sich daraus ergibt findet in erster Linie immer zwischen der Gesamtprojektleitung, federführend durch den Auftragnehmer, und dem Hersteller statt. Wenn nötig wird das ZenDiS in den Prozess mit einbezogen um möglichst schnelle und effektive Entscheidungswege zu gewährleisten. Während der Entwicklung wird die Projektleitung regelmäßig über den Fortschritt informiert. Sollte es erforderlich sein, bekommen die Hersteller Zugriff auf openDesk-Testsysteme um dort eigenständig Integrationstests durchzuführen bzw. openDesk-spezifische Integrationen frühzeitig im Entwicklungsprozess zu testen.

Um ganzheitliche Tests von openDesk mit kommenden Releases der Hersteller durchzuführen, kann auf Beta-Versionen oder Release-Candidates der jeweiligen Module zurückgegriffen werden.

Um die bestmögliche Qualität der Produkte zu gewährleisten erhalten einmal veröffentlichte Versionen nur noch Bug- und Security-Fixes. Neue Funktionen werden in der Regel nicht in bereits veröffentlichte Versionen zurückportiert. Wenn das Risiko eines Backports überschaubar ist, können Ausnahmen mit dem Hersteller im Einzelfall vereinbart werden. Im Normalfall wird eine Neuentwicklung mit der nächsten Major Version des Herstellers nach Abschluss der Entwicklung zur Verfügung gestellt. Erst wenn das neue Release des Herstellers in openDesk integriert wurde steht die Entwicklung auch in openDesk zur Verfügung.

#### Weitere Pflege der Entwicklung

Da alle Entwicklungen in das Kernprodukt fließen, werden diese nach Abschluss der Entwicklung durch die Open Source Community des Herstellers gepflegt. Es ist Aufgabe des Auftragnehmers, geeignete zentrale Kommunikationskanäle für Bug-Reports oder Änderungswünsche des ZenDiS bereitzustellen bzw. diese mit ZenDiS abzustimmen. Der Auftragnehmer nimmt Tickets auf dieser Plattform entgegen und bearbeitet sie eigenständig. Handelt es sich dabei um einen Bug oder Änderungswunsch das konkret eines der Module betrifft, dann werden diese durch die Gesamtprojektleitung über die jeweilige Entwicklungsplattform des Herstellers eingebracht. Ausnahme sind Weiterentwicklungen, welche offiziell beauftragt werden. Diese folgen dem in diesem Konzept beschriebenen Prozess.

Alles was über die öffentliche Entwicklungsplattformen der einzelnen Module läuft folgt dem normalen Open Source Community Prozess ohne garantierten SLAs seitens der Hersteller. Das gleiche gilt für Community-Beiträge in der Form von Bufixes oder Weiterentwicklungen, an denen sich alle im Sinn eines öffentlichen Entwicklungsmodells beteiligen können.

Sobald openDesk in einer produktiven Umgebung mit entsprechenden Enterprise-Subskriptionen betrieben wird, gelten für alle im Rahmen von openDesk beauftragten und erbrachten Entwicklungsleistungen die gleichen SLAs wie für das restliche Produkt. In diesem Fall stellen die Hersteller auch gesonderte Kommunikationskanäle in Form der jeweiligen Support-Systeme dem Auftragnehmer zur Verfügung. Alle Weiterentwicklungen sind automatisch in einer Subskription für openDesk enthalten.

### Austausch von einzelnen Modulen

Die Austauschbarkeit einzelner Module ist eines der zentralen Designprinzipien von openDesk. Dadurch soll zum einem ein Vendor Lock-In verhindert werden und zum anderen wird es ZenDiS so ermöglicht flexibel auf neue Anforderungen der Endanwender und auf Veränderungen im Markt oder bei den Herstellern zu reagieren. Diese Strategie wird von allen Projektpartnern in jedem Schritt der Entwicklung neuer Funktionen und insbesondere bei der Integration einzelner Module berücksichtigt.

#### Open Source, Open Standards

Folgende Grundprinzipien gewährleisten die technische Austauschbarkeit der jeweiligen Module:

- Alle Entwicklungen werden unter einer Open Source Lizenz bereitgestellt. Dadurch kann jederzeit nachvollzogen werden wie die Interaktion zwischen einzelnen Modulen umgesetzt worden ist und eine vergleichbare Integration für andere Produkte entwickelt werden.

- Bei der Integration bestehender Module greifen die Hersteller auf existierende und etablierte offene Standards zurück. Dies ermöglicht es jederzeit, andere Module über die selbe Schnittstelle anzubinden.

- Sollte es für eine gewünschte Integration noch keinen etablierten offenen Standard geben, dokumentieren die Hersteller die implementierte Schnittstelle. Dadurch ist sicher gestellt, dass zukünftige openDesk Module einen zu Punkt 2 vergleichbaren einfachen Zugang zu den Schnittstellen haben und diese entsprechend implementieren können.

Soll ein Modul von openDesk durch ein anderes Produkt ersetzt werden, müssen neben den oben genannten Voraussetzungen wie eine OSI-kompatible Open Source Lizenz und der Verwendung von offenen Standards auch folgende Aspekte berücksichtigt werden:

#### Gewährleistung der Benutzbarkeit

Die einzelnen Module von openDesk sind miteinander integriert und verfügen über ein einheitliches Look & Feel. Darüber hinaus benutzen alle openDesk-Module bestimmte Komponenten, die für alle Module vereinheitlicht wurden, wie z.B. ein Navigations-Menü.

Wird ein Modul ausgetauscht, muss sichergestellt sein, dass diese einheitliche Benutzbarkeit erhalten bleibt. Der Hersteller des betreffenden neuen Produktes muss gewährleisten, dass alle Integrationen weiterhin funktionsfähig sind und dass sich das neue Produkt nahtlos in openDesk einfügt. Darüber hinaus muss er die Funktionalität des bisherigen Moduls in einer von Anwendern mindestens ebenso einfach nutzbaren Weise zur Verfügung stellen und sollte keine Funktionen mitbringen, die in openDesk durch andere Module zur Verfügung gestellt werden, da dies eine erhebliche Verwirrung der Benutzer zur Folge hätte. Falls das neue Produkt andere Schnittstellen verwendet, müssen die verbleibenden Hersteller gegebenenfalls bestehende Integrationen anpassen oder neue Integrationen entwickeln. Das gilt auch für die CI/CD-Pipeline, die für ein neues Modul geändert oder erweitert werden muss. Des weiteren müssen kontinuierlicher Betrieb und Betriebssicherheit stets gewährleistet sein, was bedeutet, dass ggf. auch Migrationsstrategien- und Konzepte erarbeitet werden müssen, um bei Versionsupdates und -Upgrades einen sicheren und zuverlässigen Betrieb zu ermöglichen.

#### Strategie und Wirtschaftlichkeit

Trotz dieser Herausforderungen kann es im Projektverlauf oder im weiteren Produktlebenszyklus von openDesk wichtige Gründe geben, welche den Austausch von Komponenten erforderlich machen. Insbesondere wenn ein Hersteller die benötigte Komponente nicht mehr anbietet, wenn er ganz aus dem Markt verschwindet oder wenn er dauerhaft unzureichende Qualität liefert, aber auch wenn die betreffende Komponente im Gegensatz zu am Markt verfügbaren Alternativen die Erwartungen der Zielgruppe dauerhaft nicht erfüllt, ist der Austausch der betreffenden Komponente erforderlich, um den Erfolg von openDesk nicht zu gefährden.

Dazu ist folgender Prozess zu durchlaufen:

- Zunächst muss von ZenDiS als Product Owner gegenüber dem Generalunternehmer und durch diesen gegenüber den Herstellern angezeigt werden, dass ZenDiS den Austausch einer bestimmten Komponente in Erwägung zieht. Diese Mitteilung muss die auszutauschende Komponente, die geplante Alternative sowie die Begründung zum Austausch beinhalten.

- Falls der betroffene Hersteller den Austausch nicht akzeptieren möchte, hat dieser die Gelegenheit, innerhalb von vier Wochen zu der Mitteilung Stellung zu nehmen und einen Gegenvorschlag zu unterbreiten, mit dem die dem angedachten Austausch zu Grunde liegenden Ursachen ohne Austausch seines Moduls behoben werden können.

- Parallel dazu werden durch den Generalunternehmer alle übrigen Hersteller aufgefordert, zu dem angedachten Modulwechsel Stellung zu nehmen, ggf. Alternativvorschläge zu unterbreiten und ihre Aufwände, beispielsweise für die Anpassung von Schnittstellen oder die Integration von Elementen aus der Benutzeroberfläche des neuen Moduls, abzuschätzen. Eine solche Stellungnahme ist auch vom Generalunternehmer selbst anzufertigen.

- Vom Hersteller der neuen Komponente ist eine Abschätzung der Aufwände für die auf seiner Seite durchzuführenden Anpassungen etwa zur Integration in die einheitliche Menüführung, in das einheitliche Look-and-Feel von openDesk, für die automatische Übernahme und ggf. Konvertierung von Daten oder für die ggf. notwendige Schaffung von neuen Schnittstellen zu erstellen.

- Schließlich erstellt der Generalunternehmer eine Abschätzung für die Integration der neuen Komponente in CI/CD-Pipeline, Integrationstests, in das Sicherheitskonzept, Dokumentation und alle weiteren im Zusammenhang mit dem kontinuierlichen Entwicklungs- und Releaseprozess zu pflegenden Dokumente, Artefakte und Prozesse.

- Sofern sich aus dem Einsatz der neuen Komponente Änderungen in Bedienung und Funktionsumfang ergeben sollte diese mindestens drei wichtigen Nutzerorganisationen vorgestellt werden und in strukturierter Form Feedback dazu eingeholt werden.

- Sobald die entsprechenden Ergebnisse, Aufwandsabschätzungen und Preise vorliegen, kommt der Projektlenkungsausschuss (siehe Abschnitt zu Projektsteuerung) zusammen. Seine Aufgabe ist es dann, auf der Basis der entsprechenden Ergebnisse eine informierte Bewertung der Konsequenzen von Beibehaltung oder Austausch des in Frage stehenden Moduls zu bewerten und dann eine Entscheidung zu treffen. Dazu ist es erforderlich, dass mindestens entscheidungsfähige Vertreter von ZenDiS, Generalunternehmer und allen betroffenen Modulherstellern an der betreffenden Sitzung teilnehmen.

Entscheidet der Projektlenkungsausschuss für den Tausch des Moduls wird der Tausch analog eines Projektes zur funktionalen Erweiterung (siehe Abschnitt zu Projektsteuerung) durchgeführt.

### Maintainer der openDesk-Module

Die Wartung der openDesk-Module erfolgt durch folgende Maintainer/Hersteller.

#### Collabora Productivity (Ireland) Limited

* openDesk-Modul: Dokumentenbearbeitung
* Impressum: https://www.collaboraoffice.com/about-us/
* Upstream Repository Kernprodukt: https://github.com/CollaboraOnline/online
* Upstream Repository openDesk-Erweiterung:
* Release Notes:  https://www.collaboraoffice.com/release-notes/
* Entwicklungsrichtline (Contribution Guideline): https://collaboraonline.github.io/
* Fehlerberichte: Kundenticketsystem
* Produkt-Roadmap: nicht öffentlich
  
#### Element Software GmbH

* openDesk-Modul: Chat
* Impressum: [https://element.io/legal](https://element.io/legal)
* Upstream Repository Kernprodukt Synapse: [https://github.com/element-hq/synapse](https://github.com/element-hq/synapse)
* Upstream Repository Kernprodukt Element Web: [https://github.com/element-hq/element-web](https://github.com/element-hq/element-web) 
* Upstream Repository openDesk-Erweiterung:
* Release Notes: [https://github.com/element-hq/element-web/releases](https://github.com/element-hq/element-web/releases)
* Entwicklungsrichtline (Contribution Guideline):
* Fehlerberichte: [https://github.com/element-hq/element-web/issues](https://github.com/element-hq/element-web/issues)
* Produkt-Roadmap: nicht öffentlich

#### OpenProject GmbH

* openDesk-Modul: Projektmanagement
* Impressum: [https://www.openproject.org/de/rechtliches/impressum/](https://www.openproject.org/de/rechtliches/impressum/)
* Upstream Repository Kernprodukt: https://github.com/opf/openproject
* Upstream Repository openDesk-Erweiterung: https://github.com/opf/openproject-open_desk
* Release Notes: https://www.openproject.org/docs/release-notes
* Entwicklungsrichtline (Contribution Guideline): https://www.openproject.org/docs/development
* Fehlerberichte: https://www.openproject.org/docs/development/report-a-bug/
* Produkt-Roadmap: https://www.openproject.org/roadmap

#### Univention GmbH

* openDesk-Modul: Nutzerverwaltung
* Impressum: [https://www.univention.de/impressum/](https://www.univention.de/impressum/)
* Upstream Repository Kernprodukt: https://github.com/univention/univention-corporate-server / https://github.com/univention/nubus-stack 
* Upstream Repository openDesk-Erweiterung: https://github.com/univention/ums-stack-data/tree/main/helm/stack-data-swp
* Release Notes: https://www.univention.com/products/release-information/
* Entwicklungsrichtline (Contribution Guideline): https://docs.software-univention.de/manual/5.0/de/central-management-umc/policies.html
* Fehlerberichte: https://forge.univention.org/bugzilla/index.cgi
* Produkt-Roadmap: nicht öffentlich

#### Open-Xchange GmbH

* Unternehmen: https://www.open-xchange.com/

  * openDesk-Modul: E-Mail, Kalender
    * Upstream Repository Kernprodukt: https://gitlab.open-xchange.com/oss/appsuite
    * Upstream Repository openDesk-Erweiterung: https://gitlab.open-xchange.com/oss/appsuite/extensions/public-sector
    * Release Notes: https://documentation.open-xchange.com/appsuite/releases/

  * openDesk-Modul: IMAP Server 
    * Upstream Repository Kernprodukt: https://repo.dovecot.org/
    * Release Notes: Siehe NEWS File in den Sourcen, z.B. https://github.com/dovecot/core/blob/main/NEWS
    * Entwicklungsrichtline (Contribution Guideline): https://doc.dovecot.org/developer_manual/
    * Fehlerberichte: https://listen.jpberlin.de/mailman/listinfo/dovecot

#### Nordeck IT + Consulting GmbH

* openDesk-Modul: Videokonferenzlösung
* Impressum: https://nordeck.net/impressum/
* Upstream Repository Kernprodukt Whiteboard: https://github.com/nordeck/matrix-neoboard
* Upstream Repository Kernprodukt Abstimmungstool: https://github.com/nordeck/matrix-poll
* Upstream Repository Kernprodukt Meetingintegration in Element: https://github.com/nordeck/matrix-meetings
* Upstream Repository Kernprodukt Jitsi-Keycloak Adapter: https://github.com/nordeck/jitsi-keycloak-adapter
* Upstream Repository openDesk-Erweiterung: 
* Release Notes: https://github.com/nordeck/matrix-neoboard/releases (die Releasenotes der weiteren Repositories analog auf GitHub)
* Entwicklungsrichtline (Contribution Guideline): https://github.com/nordeck/.github/blob/main/docs/CONTRIBUTING.md
* Produkt-Roadmap: nicht öffentlich

#### Nextcloud GmbH

* openDesk-Modul: Dokumentenmanagement
* Impressum: https://nextcloud.com/impressum/
* Upstream Repositories Kernprodukt: https://github.com/nextcloud
* Upstream Repository openDesk-Erweiterung:https://github.com/nextcloud/integration_swp
* Release Notes: https://github.com/nextcloud/server/releases
* Entwicklungsrichtline (Contribution Guideline): https://nextcloud.com/contribute/
* Fehlerberichte: https://docs.nextcloud.com/server/latest/developer_manual/prologue/bugtracker/

#### XWiki SAS

* #### XWiki SAS

  * Impressum: [https://xwiki.com/fr/societe/mentions-legales](https://xwiki.com/fr/societe/mentions-legales)
  * openDesk-Modul: Wissensmanagement
  	* Upstream Repository Kernprodukt: https://github.com/xwiki/xwiki-platform
  	* Upstream Repository openDesk-Erweiterung: https://git.xwikisas.com/xwikisas/swp/xwiki
  	* Release Notes: https://www.xwiki.org/xwiki/bin/view/ReleaseNotes/
  	* Entwicklungsrichtline (Contribution Guideline): https://dev.xwiki.org/xwiki/bin/view/Community/Contributing
  	* Fehlerberichte: https://jira.xwiki.org/projects/XWIKI
  	* Produkt-Roadmap: https://www.xwiki.org/xwiki/bin/view/Roadmaps/
  	
  * openDesk-Modul: Diagram Editor (CryptPad)
  	* Upstream Repository Kernprodukt: https://github.com/cryptpad/cryptpad/
  	* Upstream Repository openDesk-Erweiterung: 
  	* Release Notes: https://github.com/cryptpad/cryptpad/releases
  	* Entwicklungsrichtline (Contribution Guideline): https://docs.cryptpad.org/en/how_to_contribute.html
  	* Fehlerberichte: https://github.com/cryptpad/cryptpad/issues/
  	* Produkt-Roadmap: https://cryptpad.fr/kanban/#/2/kanban/view/PLM0C3tFWvYhd+EPzXrbT+NxB76Z5DtZhAA5W5hG9wo/
