[Please find the English version here](./README_EN.md)

# Die Office und Collaboration Suite für die Öffentliche Verwaltung

Von überall aus gemeinsam mit Kolleg:innen an einem Dokument arbeiten, ohne ständig E-Mails hin und her zu schicken. Sich spontan in einer Videokonferenz zu offenen Fragen abstimmen und schnell zu einer Entscheidung kommen. Projekte team- und ressortübergreifend effizient managen oder das Wissen Einzelner allen zugänglich machen.

Ob bei der mobilen Arbeit zu Hause oder vom Schreibtisch aus dem Büro: Mit der Office & Collaboration Suite openDesk eröffnen sich für die Mitarbeiter:innen in Ämtern und Behörden diese und tausend weitere Möglichkeiten, ihre Arbeit und Zusammenarbeit neu zu gestalten und Prozesse effizienter zu steuern.

## Drei gute Gründe für openDesk

### 1. Kollaboration

openDesk ermöglicht eine effektive digitale Zusammenarbeit in der Öffentlichen Verwaltung: Erstellen von Projekten, gemeinsames Arbeiten an Dokumenten, Wiki, Chat, Videokonferenz und vieles mehr – alles aus einer Hand.

### 2. Bedarfsgerechtigkeit

openDesk ist eine bedarfsgerechte Lösung für die digitale Zusammenarbeit in der Öffentlichen Verwaltung:
Die Software wurde auf diese spezifischen Anforderungen und Bedürfnisse zugeschnitten.

### 3. Souveränität und Sicherheit

Als Open-Source-Software stärkt openDesk die Digitale Souveränität durch volle Transparenz und Kontrolle über den Quellcode. Zudem sorgt diese Off enheit für hohe Sicherheit, da Sicherheitslücken schnell erkannt und behoben werden können. Daten und Prozesse bleiben dadurch zuverlässig geschützt.

## Integration von leistungsstarken Anwendungen

openDesk integriert eine Reihe von leistungsstarken Open-Source Anwendungen unter einer Oberfläche. Für die kollaborative Arbeit in der Öffentlichen Verwaltung ergeben sich damit zahlreiche Einsatzmöglichkeiten.

Eine Übersicht sämtlicher Anwendungen ist in folgender Übersicht dargestellt.

![openDesk image](./Images/openDesk_Komponenten.png)

Einen technischeren Überblick gibt unsere [OVERVIEW](./OVERVIEW.md).

Außerdem wurde ein Informationsvideo erstellt, das einen ersten Eindruck zu openDesk und seinen Funktionen vermittelt.

![Einführung openDesk](.opencode/screenshots/openDesk_Intro.mp4){width=100%}

## Rückmeldungen und Beteiligung

openDesk wird kontinuierlich weiterentwickelt, weshalb wir uns über Verbesserungsvorschläge und Feedback freuen.
Möglichkeiten der Rückmeldung sind im Abschnitt ["Mitwirkung und Beteiligung"](https://gitlab.opencode.de/bmi/opendesk/info/-/blob/main/OVERVIEW.md?ref_type=heads#r%C3%BCckmeldungen-und-beteiligung) zu finden.
