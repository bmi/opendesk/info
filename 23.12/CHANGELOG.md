<h1>openDesk 23.12 - Changelog</h1>

The Changelog summarizes work packages that have been addressed by the suppliers in context of the project.

We keep the changelog with this focus to avoid consolidating all the changelogs for the various upstream products.

* [Ad hoc video conferences](#ad-hoc-video-conferences)
  * [Single-sign-on](#single-sign-on)
* [CryptPad](#cryptpad)
* [File share](#file-share)
  * [Accessibility](#accessibility)
  * [API OIDC authentication](#api-oidc-authentication)
  * [ICAP support](#icap-support)
* [Mail, Calendar, Contacts and Tasks](#mail-calendar-contacts-and-tasks)
  * [Accessibility](#accessibility-1)
  * [Calendar: Managed Resources](#calendar-managed-resources)
  * [Containerization](#containerization)
  * [Enterprise Contact Picker](#enterprise-contact-picker)
  * [Export emails with attachments as PDF/A](#export-emails-with-attachments-as-pdfa)
  * [Forward the attachment of an email](#forward-the-attachment-of-an-email)
  * [Mail encryption](#mail-encryption)
  * [Short-term "undo send mail"](#short-term-undo-send-mail)
  * [Message templates](#message-templates)
  * [Creation of migration tools](#creation-of-migration-tools)
* [Meeting and Collaboration](#meeting-and-collaboration)
  * [Accessibility](#accessibility-2)
  * [Calendar overview](#calendar-overview)
  * [Lobby for Element](#lobby-for-element)
  * [Recurring meetings](#recurring-meetings)
  * [Roles and permissions](#roles-and-permissions)
  * [Unified Look and Feel](#unified-look-and-feel)
  * [Whiteboard with additional features](#whiteboard-with-additional-features)
* [Office](#office)
  * [Accessibility](#accessibility-3)
  * [Calc](#calc)
  * [Writer](#writer)
  * [Interoperability](#interoperability)
  * [Overall improvements](#overall-improvements)
* [Portal and Identity Management](#portal-and-identity-management)
  * [Authorization Policy Service](#authorization-policy-service)
  * [Brute force login prevention](#brute-force-login-prevention)
  * [Containerization](#containerization-1)
  * [Provisioning concept](#provisioning-concept)
* [Projects](#projects)
  * [Accessibility](#accessibility-4)
  * [Automatic creation of service description documents](#automatic-creation-of-service-description-documents)
  * [Enhancements of the OpenProject-Nextcloud integration](#enhancements-of-the-openproject-nextcloud-integration)
  * [German master and demo data](#german-master-and-demo-data)
  * [iCalendar support](#icalendar-support)
  * [Integration of agenda and protocols](#integration-of-agenda-and-protocols)
* [XWiki](#xwiki)
  * [Accessibility Element](#accessibility-element)
  * [Authentication](#authentication)
  * [Quick Actions](#quick-actions)
  * [Upgrades](#upgrades)
  * [Usability](#usability)

## Ad hoc video conferences

### Single-sign-on

Jitsi is provided as an ad hoc video conferencing option with single-sign-on integration into openDesk for moderators. External users can join the conference using a link without the need for authentication.

## CryptPad

Cryptpad was added to openDesk to support the creation and editing of Draw.IO files including features like collaborative real-time editing of these files.

Cryptpad also provides the framework for upcoming real-time editor use cases.

## File share

### Accessibility

Accessibility has been improved to make Nextcloud more compliant with accessibility requirements.

### API OIDC authentication

The Nextcloud APIs support now [Open ID Connect](https://en.wikipedia.org/wiki/OpenID#OpenID_Connect_(OIDC)) authentication.

### ICAP support

To support malware scanning for uploaded files ICAP support was implemented.

## Mail, Calendar, Contacts and Tasks

### Accessibility

Open-Xchange AppSuite has been improved to align the requirements of accessibility.

### Calendar: Managed Resources

The booking of a resource, for example, a meeting room, can be managed by defined users. When a resource is booked, the authorized users receive an email with the booking details and can approve or reject the booking. The user who initiated the booking receives a notification with the booking's status.

### Containerization

The Open-Xchange AppSuite8 is containerized and supports K8s deployments.

### Enterprise Contact Picker

The Enterprise Contact Picker provides access to contacts and users from the openDesk identity management's directory service. It allows easy access to multiple address lists, e.g. separated by organization, with support search and filter options.

### Export emails with attachments as PDF/A

Export an e-mail including attachments as a PDF/A file. The text of the e-mail (plain/HTML) is added first to the PDF and the attachments are converted and added to the following pages of the PDF.

### Forward the attachment of an email

A simplified method to insert attachments from an existing e-mail into the creation dialogue of a new mail.

### Mail encryption

OX Guard already supported [OpenPGP](https://www.openpgp.org/) for sending and receiving end-to-end encrypted emails and was enhanced to also support [S/MIME](https://en.wikipedia.org/wiki/S/MIME).

### Short-term "undo send mail"

The sending of an email is delayed for a couple of seconds and allows users to change their mind and protect users from accidentally sending emails e.g. to the wrong recipient.

### Message templates

It is now possible to create message templates and insert these into your mail for efficiency in case of repetitive requests.

### Creation of migration tools

The mailing module offers the possibility to have a look (free-busy) into other mailing applications like Outlook.

## Meeting and Collaboration

### Accessibility

Element was optimized to align with the requirements of accessibility.

### Calendar overview

The calendar function has been optimized to address common calendar functions, and different calendar views (day/week/month).

### Lobby for Element

A virtual lobby has been implemented from which guests can request to join a meeting and the meeting owner can decide to accept these requests.

### Recurring meetings

Improved management for recurring meetings supporting modification of individual appointments.

### Roles and permissions

A permission concept for Element and the openDesk widgets has been implemented to fit the overall openDesk use case and allow better differentiation between various roles:
- "Admin" and "Moderator"
- "User" and "Guest"

### Unified Look and Feel

The Element user interface theme was adapted in color, font, and appearance to fit the overall openDesk branding.

### Whiteboard with additional features

The whiteboard has been enhanced:

- Collaborative simultaneous editing
- Export and import functionality
- Multi-page support
- Presentation mode avoiding changes by other participants
- Display a grid and arrange items in the grid
- Users can create multiple slides and add content. An undo function is available, as well as a copy-and-paste function.

## Office

### Accessibility

To improve the accessibility of the component various topics have been addressed, e.g.:

- Screen reader support
- Document navigation
- Keyboard shortcuts
- Dark mode

### Calc

- Support for 64-bit zip files
- A compact view of pivot tables

### Writer

- Multi-page floating frames and tables
- Circular table navigation
- Multi-page floating frames
- Improvements in the anchoring of floating shapes
- A more advanced justification process

### Interoperability

To improve interoperability with other programs various optimizations have been implemented.

### Overall improvements

- Document themes
- Overall ergonomic improvements
- Page number insertion wizard
- Multi-stop gradients

## Portal and Identity Management

### Authorization Policy Service

To prepare the implementation of a fine-grained permissions concept for end-user use cases, a data model and a policy service were implemented that can provide authorization information to APIs.

### Brute force login prevention

An add-on to the Univention Keycloak that prevents brute force attacks on the openDesk login by detecting this attack type and taking countermeasures like rendering a [CAPTCHA](https://en.wikipedia.org/wiki/CAPTCHA)[^1], blocking the device or blocking the IP address the attack originates from.

### Containerization

The relevant services of the Univention Corporate Server have been transitioned into a set of containers that serve as openDesk Portal and Identity Management System, including the Univention Keycloak supporting group-based [TOTP](https://en.wikipedia.org/wiki/Time-based_one-time_password) two-factor authentication.

### Provisioning concept

[The concept](./Konzepte/Provisioning%20Konzept%200.11.pdf) describes a new service for connecting and synchronizing user, group and asset objects that are managed in the central identity and access management system with other components of the stack that have. The concept derives the general criteria from the openDesk architecture concept.

## Projects

### Accessibility

OpenProject was optimized to align with the requirements of accessibility.

### Automatic creation of service description documents

Improve the PDF export of project management objects based on the [project management guidelines for the public sector](https://www.bmi.bund.de/SharedDocs/downloads/DE/veroeffentlichungen/themen/moderne-verwaltung/praxisleitfaden-projektmanagement.html).

### Enhancements of the OpenProject-Nextcloud integration

The existing integration between OpenProject and Nextcloud was enhanced with features like:

- Automatically manage Nextcloud group folders alongside projects (create/update/archive/delete).
- Automatically manage access to the aforementioned group folder based on the members of the related project.
- Support for folder structure templating.
- Support for creating a project work package from Nextcloud.

### German master and demo data

OpenProject comes now with translated (German) demo project data.

### iCalendar support

Allow a recurring import of relevant project dates into other modules (e.g. mail & calendar) by providing an [iCalendar](https://en.wikipedia.org/wiki/ICalendar) interface.

### Integration of agenda and protocols

It is now possible to create a specific agenda for meetings with detailed topics, timeline and responsibility. You can also fill in the protocol into your prepared agenda and provide it to the other participants.

## XWiki

### Accessibility Element

XWiki was optimized to align with the requirements of accessibility.

### Authentication

Support for the [OpenID Connect](https://en.wikipedia.org/wiki/OpenID#OpenID_Connect_(OIDC)) based single-sign-on in the XWiki community edition.

### Quick Actions

Support for quick actions triggered by the user entering the "/" character. Quick actions support formatting (title/bold/italic) and inserting options (tables).

### Upgrades

XWiki upgrades no longer require manual interaction by an administrator for each subwiki.

### Usability

The notification features have been improved. A modern layout has been added. Real-time support for inline editing for standard pages has been improved.

[^1]: Not enabled in openDesk by default.
