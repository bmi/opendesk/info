**_Please find the English version [here](./OpenProject_as_part_of_openDesk_EN.md)_.**

# OpenProject: Die Projektmanagement-Komponente für openDesk - Was bietet die Open-Source-Software?

**openDesk - Der Souveräne Arbeitsplatz** hat das Ziel, insbesondere für die öffentliche Verwaltung eine echte Open-Source-Alternative zu proprietären Herstellern zu bieten.  Wir möchten Sie hier weiterhin über dieses spannende Projekt auf dem Laufenden halten.

Die einzelnen Software Komponenten von openDesk decken verschiedene alltägliche Arbeitsbereiche ab. So wird beispielsweise die Open-Source-Software OpenProject für den Bereich des Aufgaben- und Projektmanagements eingesetzt. Für openDesk setzt das Berliner Unternehmen Funktionen auf den Entwicklungsplan, die besonders für den öffentlichen Sektor sinnvoll sind. Dazu gehören beispielsweise eine verbesserte Termin-Dokumentation, ein professioneller PDF-Export oder ein hoher Kontrast-Modus für visuell beeinträchtigte Nutzer:innen.

Wir wollen OpenProject genauer unter die Lupe nehmen: Was ist alles mit der Software möglich, ob als Standalone-Variante oder als Modul von openDesk – Der Souveräne Arbeitsplatz?

## Einsatzbereiche von OpenProject

**OpenProject** ist eine Software zur Steuerung von Projekten, die unter der Open-Source-Lizenz GNU GPL v3 steht. Sie ist für vielfältige Projektmanagement-Methoden wie Wasserfall, agil oder hybrid geeignet und fördert die Teameffizienz über alle Phasen eines Projekts hinweg. Somit können Organisationen jeder Größe und Branche OpenProject für ihre Projekte einsetzen. Während einige Plattformen zusätzliche Plugins für erweiterte Funktionen benötigen, zielt OpenProject darauf ab, einen umfassenden Satz von Funktionen innerhalb eines einzigen Systems anzubieten.

Die Community Edition ist kostenfrei und zur Selbstinstallation verfügbar. Zusätzlich bietet die OpenProject GmbH Support und erweiterte Funktionen für Enterprise-Kunden. Öffentliche Einrichtungen sind nicht erst seit Projektstart von openDesk eine wichtige Nutzergruppe von OpenProject. Für gemeinnützige Organisationen und Bildungseinrichtungen sind außerdem Sonderkonditionen möglich.

**Kernanwendungsfälle von OpenProject sind:**

- Verwaltung des Projektportfolios
- Termin- und Ressourcenplanung
- Aufgabenverwaltung
- Agiles Projektmanagement wie Kanban und Scrum
- Anforderungsmanagement und Releaseplanung
- Nachverfolgung von Zeit und Budget
- Gemeinsames Erstellen von Agenden und Protokollen

Besonderen Wert legt das Unternehmen auf Aspekte wie Datenschutz, Sicherheit und Datensouveränität.

![Screenshot aus der OpenProject-Anwendung](../_media/openproject-screenshot-work-package-gantt-filter.png)

## Highlight-Funktionen von OpenProject

- **Arbeitspakete:** Diese Funktion zentralisiert alle Aspekte einer Aufgabe, eines Meilensteins oder eines Problems und bietet eine einzige Quelle der Wahrheit für eine einfachere Verfolgung und Verantwortlichkeit.

- **Gantt-Charts:** Kollaboratives Erstellen von Projektplänen und Scheduling.

- **Agile Boards:** OpenProject unterstützt agile Methoden wie Scrum und Kanban, ohne dass zusätzliche Plugins erforderlich sind.

- **Termine:** Erstellen von Agenden und Protokollen und direktes Verlinken mit zugehörigen Arbeitspaketen eines Projektes.

- **Grundlinie (Planungsvergleich):** Diese Funktion ermöglicht es Ihnen, den aktuellen Status eines Projekts mit früheren Zuständen zu vergleichen, was das Verfolgen von Änderungen und Leistungen erleichtert.

- **Integrierte Zeit- und Kostenerfassung:** Im Gegensatz zu vielen anderen Tools enthält OpenProject sowohl eine Zeit- als auch eine Kostenerfassung, was für die Budgetverwaltung entscheidend sein kann.

- **Rollenbasierte Zugriffskontrolle:** Hochgradig anpassbare Rollen und Berechtigungen ermöglichen klare Rollendefinitionen und Zugriffsbeschränkungen.

Wenn Sie mehr über die Anwendungsfälle und Funktionen von OpenProject erfahren möchten, werfen Sie [hier](https://www.openproject.org/de/docs/) einen Blick auf die Produktübersicht der Software.