**_Die Deutsche Version finden Sie [hier](./XWikiandCryptPad_as_part_of_openDesk_DE.md)._**

# XWiki and CryptPad: The knowledge management and diagram components for openDesk - What do the open-source software offer?

The basic version of **openDesk - The Sovereign Workplace** is composed of several open-source components used for everyday work of the German government public administration. The XWiki platform is used, for example, for knowledge management, collaboration, and creation of collaborative web applications. For openDesk, XWiki SAS (top sponsoring company for the XWiki platform) is developing features aimed to support the work of the Public Sector, such as Quick Actions, real-time editing, WCAG compliance, while bringing improvements to design, security, and deployment. Moreover, openDesk benefits also from CryptPad's diagram capabilities.

## Areas of application for XWiki

**XWiki** is a powerful open-source wiki platform that offers extensive customization features, collaborative editing, document and files management, integration capabilities, and granular rights management. It is a flexible and scalable solution, with many robust features for creating, organizing, searching, and sharing knowledge within organizations and runs under the LGPLv2.1 license. This means that individuals and organizations of all sizes are able to structure information and save time and money while enhancing collaboration on both team and organizational levels.

Being focused on efficient communication and collaboration, XWiki redefines the value of business knowledge by allowing any user to access critical information in a fast and efficient way while reducing organizational silos. With over 800 extensions, applications, macros, skins, and plugins available, XWiki is one of the most flexible and versatile collaborative wiki platforms out there.
Please find more information [here](https://www.xwiki.org/xwiki/bin/view/Main/).

**The use cases of XWiki are:**

-	Internal and/or external knowledge base
-	Documentation
-	Procedures management
-	Extranet and communities
-	Digital Workplace
-	Self-service desk
-	Public website
-	Content management system

![Screenshot from the XWiki application](../_media/XWiki-in-openDesk.png)
_XWiki page example from: organize, share knowledge, and collaborate securely across your organization._

## Highlight features of XWiki

-	**Hierarchical organization:** XWiki provides a hierarchical organization of structured content with nested pages and sub-wikis, facilitating a structured and organized content management system
-	**Versatile editing options:** Enjoy seamless editing with XWiki through WYSIWYG, Markdown, and wiki syntax editors, providing users with flexibility and choice in content creation
-	**Version control and tracking:** XWiki offers version rollback, history tracking, and version comparison features, ensuring a robust version control system for collaborative content development
-	**Comprehensive navigation features:** Explore comprehensive navigation features such as search, page index, breadcrumb, URL linking, and jump-to-page, providing users with diverse ways to access and find information
-	**Scripting and automation:** Take advantage of XWiki's scripting and automation capabilities to streamline tasks and enhance efficiency in content management processes
-	**Extensions:** XWiki supports the use of extensions plugins, which can be added to extend its functionality. These extensions can include things like macros, applications, and more
-	**User and group rights management:** XWiki ensures robust user and group rights management, allowing administrators to control access at various levels, including by space, page, and user groups of who can view, edit, or manage different parts of the wiki

If you would like to find out more about how XWiki works and learn more about the features and use cases, take a look at the software's product overview on [xwiki.org](https://www.xwiki.org/xwiki/bin/view/Documentation/UserGuide/Features/).

## Areas of application for CryptPad

**CryptPad** is an open-source online collaboration suite that provides secure and privacy-focused tools for document editing, real-time collaboration, and communication. It is designed with a strong emphasis on user privacy and data security. CryptPad enables users to collaborate and share rich text, spreadsheets, polls, presentations, whiteboard functions, forms, Kanban, diagrams, and code/markdown without compromising the confidentiality of their data.

**The use cases of CryptPad are:**

-	Secure content collaboration
-	Digital Workspace
-	Secure file share & sync

In the scope of openDesk, CryptPad has been integrated only with the Diagram module and the ["Open in CryptPad"](https://apps.nextcloud.com/apps/openincryptpad) application in Nextcloud. The configuration done for deployment allows opening, editing, and saving collaboratively and securely diagrams stored on Nextcloud, from CryptPad.

![Screenshot from the CryptPad application](../_media/CryptPad-diagram-in-openDesk.png)
_CryptPad diagram example from openDesk: real-time encrypted collaboration for your teams._

## Highlight features for CryptPad

-	**End-to-End Encryption:** CryptPad employs end-to-end encryption, meaning that the data is encrypted on the client side before it is transmitted to the server. This ensures that even the service providers cannot access the content
-	**Real-Time Collaboration:** Users can collaborate in real-time on documents, spreadsheets, presentations, and more. Changes made by multiple users are synchronized instantly
-	**Privacy-Focused:** CryptPad is designed to minimize the collection of personal information. Users can access the service without creating an account, and even when accounts are used, minimal information is stored
-	**Diverse Applications:** CryptPad offers a variety of applications, including a rich text editor, spreadsheet editor, presentation editor, code editor, whiteboard, and more
-	**Access Control:** Users have control over who can access their documents. They can set permissions to make documents private, share them with specific individuals, or make them public
-	**Self-Hosted Option:** Users who are particularly concerned about data privacy can choose to self-host CryptPad, running their own instance of the service on their servers


Find further information about CryptPad [here](https://cryptpad.fr/).