**_Please find the English version [here](./XWikiandCryptPad_as_part_of_openDesk_EN.md)_.**

# XWiki und CryptPad: Die Wissensmanagement- und Diagrammkomponenten für openDesk - Was bietet die Open-Source-Software?

Die Basisvariante von **openDesk - Der Souveräne Arbeitsplatz** besteht aus mehreren Open-Source-Komponenten, die für die tägliche Arbeit der Öffentlichen Verwaltung (ÖV) in Deutschland verwendet werden. Die XWiki-Plattform wird zum Beispiel für Wissensmanagement, Zusammenarbeit und die Erstellung von kollaborativen Webanwendungen verwendet. Für openDesk entwickelt XWiki SAS (Hauptsponsor der XWiki-Plattform) Funktionen, die die Arbeit des öffentlichen Sektors unterstützen sollen, z. B. Quick Actions, Echtzeitbearbeitung, WCAG-Konformität, und bringt gleichzeitig Verbesserungen bei Design, Sicherheit und Bereitstellung. Darüber hinaus profitiert openDesk auch von den Diagrammfunktionen von CryptPad.

## Anwendungsbereiche für XWiki

**XWiki** ist eine leistungsstarke Open-Source-Wiki-Plattform, die umfangreiche Anpassungsfunktionen, gemeinsame Bearbeitung, Dokumenten- und Dateiverwaltung, Integrationsmöglichkeiten und eine granulare Rechteverwaltung bietet. Es ist eine flexible und skalierbare Lösung mit vielen robusten Funktionen für die Erstellung, Organisation, Suche und den Austausch von Wissen innerhalb von Organisationen und läuft unter der LGPLv2.1 Lizenz. Das bedeutet, dass Einzelpersonen und Organisationen jeder Größe in der Lage sind, Informationen zu strukturieren, Zeit und Geld zu sparen und gleichzeitig die Zusammenarbeit auf Team- und Organisationsebene zu verbessern.

XWiki ist auf effiziente Kommunikation und Zusammenarbeit ausgerichtet und definiert den Wert von Unternehmenswissen neu, indem es allen Benutzenden ermöglicht, schnell und effizient auf wichtige Informationen zuzugreifen und gleichzeitig organisatorische Silos abzubauen. Mit mehr als 800 verfügbaren Erweiterungen, Anwendungen, Makros, Skins und Plugins ist XWiki eine der flexibelsten und vielseitigsten Plattformen für kollaborative Wikis, die es gibt.
Weitere Informationen gibt es [hier](https://www.xwiki.org/xwiki/bin/view/Main/).

**Die Anwendungsfälle von XWiki sind:**

-   Interne und/oder externe Wissensbasis
-	Dokumentation
-	Verwaltung der Verfahren
-	Extranet und Gemeinschaften
-	Digitaler Arbeitsplatz
-	Selbstbedienungsschalter
-	Öffentliche Website
-	Inhaltsverwaltungssystem

![Screenshot aus der XWiki-Anwendung](../_media/XWiki-in-openDesk.png)
_Beispiel für eine XWiki-Seite von openDesk: Organisation, Wissensaustausch und sicheres Zusammenarbeiten in Ihrer Organisation._

## Wichtigste Funktionen von XWiki

-   **Hierarchische Organisation:** XWiki bietet eine hierarchische Organisation von strukturierten Inhalten mit verschachtelten Seiten und Unter-Wikis, die ein strukturiertes und organisiertes Content-Management-System ermöglichen
-	**Vielseitige Bearbeitungsoptionen**: Genießen Sie die nahtlose Bearbeitung mit XWiki durch WYSIWYG-, Markdown- und Wiki-Syntax-Editoren, die den Benutzern Flexibilität und Auswahl bei der Erstellung von Inhalten bieten
-	**Versionskontrolle und -verfolgung:** XWiki bietet Funktionen zur Versionsrückverfolgung, zur Nachverfolgung der Historie und zum Versionsvergleich und stellt damit ein robustes Versionskontrollsystem für die gemeinschaftliche Entwicklung von Inhalten sicher
-	**Umfassende Navigationsfunktionen:** Entdecken Sie umfassende Navigationsfunktionen wie Suche, Seitenindex, Breadcrumb, URL-Verknüpfung und Seitensprung, die den Benutzenden verschiedene Möglichkeiten bieten, auf Informationen zuzugreifen und sie zu finden
-	**Skripting und Automatisierung:** Nutzen Sie die Skript- und Automatisierungsfunktionen von XWiki, um Aufgaben zu rationalisieren und die Effizienz von Content-Management-Prozessen zu steigern
-	**Erweiterungen:** XWiki unterstützt die Verwendung von Erweiterungs-Plugins, die hinzugefügt werden können, um seine Funktionalität zu erweitern. Diese Erweiterungen können beispielsweise Makros, Anwendungen und mehr umfassen
-	**Verwaltung von Benutzer- und Gruppenrechten:** XWiki bietet eine robuste Verwaltung von Benutzer- und Gruppenrechten, die es Administrator:innen ermöglicht, den Zugriff auf verschiedenen Ebenen zu steuern, einschließlich nach Bereich, Seite und Benutzergruppen, die verschiedene Teile des Wikis anzeigen, bearbeiten oder verwalten können

Wenn Sie mehr darüber erfahren wollen, wie XWiki funktioniert und mehr über die Funktionen und Anwendungsfälle lernen möchten, werfen Sie einen Blick auf die Produktübersicht der Software auf [xwiki.org](https://www.xwiki.org/xwiki/bin/view/Documentation/UserGuide/Features/).

## Anwendungsbereiche für CryptPad

**CryptPad** ist eine Open-Source-Suite für die Online-Zusammenarbeit, die sichere und datenschutzfreundliche Tools für die Bearbeitung von Dokumenten, die Zusammenarbeit in Echtzeit und die Kommunikation bietet. Bei der Entwicklung wurde besonderer Wert auf den Datenschutz und Datensicherheit gelegt. CryptPad ermöglicht die Zusammenarbeit und den Austausch von Rich Text, Tabellenkalkulationen, Umfragen, Präsentationen, Whiteboard-Funktionen, Formularen, Kanban, Diagrammen und Code/Markdown, ohne die Vertraulichkeit der Daten zu gefährden.

**Die Anwendungsfälle von CryptPad sind:**

-	Sichere Zusammenarbeit bei Inhalten
-	Digitaler Arbeitsbereich
-	Sichere Dateifreigabe und Synchronisierung

Im Rahmen von openDesk wurde CryptPad nur in das Diagramm-Modul und die Anwendung ["Open in CryptPad"](https://apps.nextcloud.com/apps/openincryptpad) in Nextcloud integriert. Die für die Bereitstellung vorgenommene Konfiguration ermöglicht das gemeinsame und sichere Öffnen, Bearbeiten und Speichern von Diagrammen, die auf Nextcloud gespeichert sind, von CryptPad aus.

![Screenshot aus der CryptPad-Anwendung](../_media/CryptPad-diagram-in-openDesk.png)
_Beispiel für CryptPad-Diagramm von openDesk: verschlüsselte Zusammenarbeit in Echtzeit für Teams._

## Hervorzuhebende Funktionen für CryptPad

-	**Ende-zu-Ende-Verschlüsselung:** CryptPad setzt eine Ende-zu-Ende-Verschlüsselung ein, d.h. die Daten werden auf der Client-Seite verschlüsselt, bevor sie an den Server übertragen werden. Dadurch wird sichergestellt, dass auch Dienstleister keinen Zugriff auf die Inhalte haben
-	**Zusammenarbeit in Echtzeit:** Benutzende können in Echtzeit an Dokumenten, Tabellenkalkulationen, Präsentationen und mehr zusammenarbeiten. Änderungen, die von mehreren Benutzenden vorgenommen werden, werden sofort synchronisiert
-	**Datenschutzfokussierung:** CryptPad ist so konzipiert, dass möglichst wenig persönliche Daten gesammelt werden. Benutzende können auf den Dienst zugreifen, ohne ein Konto einzurichten, und selbst wenn Konten verwendet werden, werden nur minimale Informationen gespeichert
-	**Vielfältige Anwendungen:** CryptPad bietet eine Vielzahl von Anwendungen zur Bearbeitung von Dateien und unterstützt unter anderem Rich-Text, Tabellen, Präsentationen und Code
-	**Zugriffskontrolle:** Benutzende haben die Kontrolle darüber, wer auf ihre Dokumente zugreifen kann. Sie können Berechtigungen festlegen, um Dokumente privat zur Verfügung zu stellen, sie für bestimmte Personen freizugeben oder sie öffentlich zugänglich zu machen
-	**On-Premise:** Benutzende, denen der Datenschutz besonders am Herzen liegt, können CryptPad selbstständig auf eigenen Servern betreiben

Finden Sie mehr über CryptPad [hier](https://cryptpad.fr/).
