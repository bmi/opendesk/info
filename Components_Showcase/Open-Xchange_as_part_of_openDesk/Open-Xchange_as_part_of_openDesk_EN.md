**_Die Deutsche Version finden Sie [hier](./Open-Xchange_as_part_of_openDesk_DE.md)._**

# Open-Xchange: The E-Mail Component for openDesk - What Does the Open Source Software Offer?

Open-Xchange was founded in 2005 with the philosophy and goal of keeping the Internet free and open as it was originally intended. Since then, we have developed software that helps our communities, partners and customers to provide outstanding and competitive services.
Open-Xchange has always focused on email and today offers the world's most popular email services, from email servers (DoveCot) to email platforms (OX App Suite). We owe this success not only to the entire Open-Xchange team, two thirds of whom are developers, but also to our great partners and over 220 million users worldwide.

## OX App Suite in use

The OX App Suite, which was originally released in 2005 as Open-Xchange Hosted Edition, is more than just an email platform. It is a modular email suite and can therefore be perfectly combined with other products to create a superordinate platform. The large number of integrated modules include, among others:

- **Sophisticated email solution** designed for both personal and business users, offering features such as multiple inbox management, delegation, email templates and even the use of artificial intelligence.
- **An innovative calendar** that meets the needs of private and business users and combines visual appointment management, multiple personal, shared and public calendars, time zone management, resource management and delegation options.
- **Online storage** that is more than just online file storage. It includes drag & drop on the desktop, thumbnail and full previews, online file editing, file versioning and third-party storage integration. A highlight is email integration, allowing users to easily send links instead of large attachments.
- **E-mail and file encryption** that is the most user-friendly and secure encryption available. With a click of the mouse, users can first encrypt a complete e-mail (e-mail and attachments) and then encrypt the profile transparently. Encryption that works because it is so simple.

![Screenshot from the OX application](../_media/OX%20App%20Suite%20as%20a%20product.png)

**The core use cases are**

-	External and internal communication via email
-	Secure mail dispatch
-	Extensive and intuitive appointment and calendar functions
-	Professional management of contacts and groups

**The open-Xchange App Suite with the following features is used as part of the _openDesk suite_:**

- Email attachments can be transferred directly from the "Files" app of openDesk
- Email attachments can be saved directly in the "Files" app of openDesk
- Files can also be inserted into an email as a download link. This makes it easy to send large or sensitive files.
- Access to files via a download link can be limited in time and protected with a password. This further increases security.
- A link for a video conference can be created directly for a calendar entry.

## The highlight functions include

- **New design:** OX App Suite has been given a brand new user interface. We have worked on countless details to offer you a great and contemporary user experience.

- **New search:** As one of the most used features, we have simplified the search function and moved it to the top center of the navigation area. It is a simple and easy to use input field with an additional dropdown in case more complex search filters are needed.

- **Undo send:** "Undo send" is a function that allows you to cancel sending an email shortly after you have clicked on the "Send" button. This function is extremely useful if you have made a mistake, e.g. forgotten to attach a file or sent an e-mail to the wrong recipient or simply clicked on "Send" by mistake.

- **Dark mode:** Let it be dark! In dark mode, light text is displayed on a dark background instead of the usual black text on a white background. In addition to personal preference, dark mode has advantages: less light is generally required, which improves legibility in dark environments and is easier on the eyes.

- **Themes:** Let there be color! Do you like purple? Or would you prefer orange? The choice is yours! There are several themes with popular colors to choose from, which you can combine with different backgrounds.

- **Halo View:** You can view all your contact information with a single click, exactly when you need it. This includes the shared e-mail history or shared calendar appointments.

- **Managed resources:** The "Managed resources" function enables companies to efficiently manage and schedule shared resources such as conference rooms and devices. This feature ensures that resources are used effectively and efficiently, increasing productivity and reducing scheduling conflicts.

- **Meeting reminder:** To help you keep track of your schedule, we've added a countdown to remind you of your upcoming meetings with color, sound and desktop notifications.

