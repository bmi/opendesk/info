**_Die Deutsche Version finden Sie [hier](./Nextcloud_as_part_of_openDesk_DE.md)._**

# Nextcloud-Hub - What Does the Open Source Software Offer?

## Secure collaboration with Nextcloud in openDesk

In openDesk, Nextcloud provides real-time access to files with desktop synchronization, mobile access and the ability to edit files, share them, comment on them and track activities. Nextcloud offers a user-friendly experience following high standards for accessibility throughout its web interface, desktop clients, and mobile apps, ensuring real-time collaboration and seamless data access across all devices, regardless of location.

**Key features of Nextcloud Files:**

-	Secure file and folder sharing
-	Flexible access permissions
-	Reinforced link protection and expiration dates
-	File locking to protect files from alteration while editing
-	Desktop and mobile synchronization
-	Document editing and co-editing via office suite integration
-	Ownership transfer for files and folders
-	Version control with ability to restore old versions
-	Client-side protection of files and end-to-end encryption
-	File Drop feature to let others upload files to a secure folder via link
-	File property configuration for any number of users or groups

![Screenshot from Nextcloud-Files](../_media/1%20Nextcloud%20Files.png)

## Nextcloud: Integrated collaboration platform with privacy at its core

_Nextcloud_ develops comprehensive and versatile solutions that aim to provide a secure, private, and controlled environment for file synchronization and sharing, team collaboration, and productivity. The full-stack platform Nextcloud Hub is a 100% open-source, self-hosted solution easy to deploy and integrate into existing storage and user directories as a singular or federated instance.

The full Hub includes four key app modules, providing a unified platform for collaborative workflows:

-	Nextcloud Files: file sync & share platform for secure document management and sharing.
-	Nextcloud Groupware: a bundle of productivity solutions for team collaborationL Calendar, Contacts, Mail and Deck.
-	Nextcloud Talk secure web conferencing and chat with high performance back end and SIP bridge.
-	Nextcloud Office: Nextcloud Text markdown editor and collaborative office suite built with Collabora Online.

_**openDesk includes Nextcloud Files, while Talk, Groupware and Office functionalities are provided by other vendors.**_

![Screenshot from Nextcloud-Files](../_media/2_Nextcloud_Files.png)

Modular architecture allows integrating Hub with third-party apps including CRM software, office suites, project management tools, and more.
Over 200 ready apps are currently available for integration.

The platform prioritizes security with robust encryption features, rule-based file access control, stringent password policies and brute-force protection. The simplicity of configuration and integration not only lowers costs but also reduces risks, allowing organizations to leverage their existing IT investments effectively.

Nextcloud Hub fits all teams that need to securely access and share data among colleagues and with customers or partners. Organizations using Nextcloud span various industries and provide necessary compliance tools for regulated sectors such as finance, government, education, healthcare, and more.

Overall, Nextcloud Hub aims to provide a holistic collaboration platform with a focus on security, privacy, and control over the data without vendor lock-in and compliance risks.

## How Nextcloud helps build a private, sovereign platform

Deploying Nextcloud allows organizations to prioritize data safety by housing it within trusted data servers in their country, ensuring control over data without concerns about foreign regulations.
In the face of growing digitization, Nextcloud's secure document exchange features, such as File Drop, group folders with advanced permissions, and persistent versioning, offer a solution that guarantees the privacy and security of data at every step of processing.

-	Full compliance with important regional and industrial regulations including GDPR, CCPA, HIPAA, FERPA, and COPPA.
-	Secure on-premises deployment
-	100% open source code
-	Consistent update cycle and proactive approach to vulnerability analysis
-	Local data within organization’s legal perimeter
-	Secure sharing and collaboration with flexible permissions, E2EE, File Drop, and other protection tools with comfortable user experience in mind.






