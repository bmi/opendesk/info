**_Die Deutsche Version finden Sie [hier](./Nubus_as_part_of_openDesk_DE.md)._**

With Nubus, Univention provides the central **identity and access management system** (IAM) for openDesk as well as the **web portal** through which all openDesk applications can be accessed. The Univention components ensure the orchestrated interaction of the individual applications and, thanks to their open IT architecture with standardized interfaces, allow further applications to be integrated into openDesk in the future.

# Identity & Access Management (IAM)

Univention's IAM enables the central administration of objects relevant to openDesk such as

- Users
- Groups
- Functional mailboxes
- Resources

User accounts are administered centrally via a web-based console and administrators can manage the associated access rights based on group memberships. Univention's identity management system also offers standardized interfaces for importing and exporting users, groups and authorizations from other directory services. This eliminates the need for manual or double data maintenance, significantly reducing administration work and thus increasing data quality.

# User Web Portal

The Univention web portal enables users to access all applications connected to openDesk centrally and conveniently after logging in. Just one click on the corresponding tile and the desired service, be it mail, calendar, documents or video conferencing, opens in its own tab.

Thanks to a self-service function, users can also edit their own profile data and, for example, change contact details or add a profile picture. They can reset and change their password themselves using the password reset function.

![Screenshot from the Nubus web portal in openDesk](../_media/Nubus-oD-Portal_EN.png)

# Identity Provider for Single Sign-on, Single Logout and Two-Factor Authentication

The basis for access via a single sign-on, i.e. a single login with a user account and password, is the connection to an identity provider. Univention's IAM generally also enables the connection to external identity providers.

The IAM implements a (login) brute force prevention to defend against cyber attacks and misuse of user profiles. Login security is also enhanced by two-factor authentication that can be activated on a group basis using time-based one-time passwords (TOTP).

# Kubernetes-based Cloud Technology

Univention's components for IAM, the integration of the various IT components and the portal are implemented as containerized Kubernetes components, so that openDesk can be operated both in the company's own data center and as a cloud service with a hosting provider.

More information on Univention's open source solutions for identity and access management can be found on the website [www.univention.de](https://www.univention.de).
