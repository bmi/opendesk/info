**_Please find the English version [here](./Nubus_as_part_of_openDesk_EN.md)_.**

Univention stellt für openDesk mit Nubus das zentrale **Identitäts- und Zugriffsmanagementsystem** (IAM) sowie das **Web-Portal** bereit, über das der Zugriff auf alle openDesk Anwendungen erfolgt. Die Univention-Komponenten sorgen für das orchestrierte Zusammenspiel der einzelnen Anwendungen und erlauben durch ihre offene IT-Architektur mit standardisierten Schnittstellen zukünftig weitere Anwendungen in openDesk zu integrieren.

# Identity & Access Management (IAM)

Das IAM von Univention ermöglicht die zentrale Administration der für openDesk relevanten Objekte wie:
- User
- Gruppen
- Funktionspostfächer
- Ressourcen

Die Accounts der Nutzenden werden an zentraler Stelle über eine webbasierte Konsole administriert und auf Basis von Gruppenzugehörigkeiten können Administrierende die zugehörigen Zugriffsrechte verwalten. Das Identity Managementsystem von Univention bietet außerdem standardisierte Schnittstellen für den Im- und Export von Benutzern, Gruppen und Berechtigungen aus anderen Verzeichnisdiensten. Manuelle oder doppelte Pflege von Daten wird so überflüssig, Administrationsaufwände werden signifikant reduziert und damit die Datenqualität erhöht.

# Benutzer-Web-Portal

Das Web-Portal von Univention ermöglicht Nutzenden nach Anmeldung, zentral und komfortabel auf alle an openDesk angebundene Anwendungen zuzugreifen. Nur ein Klick auf die entsprechende Kachel und der gewünschte Dienst, sei es Mail, Kalender, Dokumente oder Videokonferenzen, öffnet sich in einem eigenen Tab.

Dank eines Self-Service können die Nutzenden außerdem die eigenen Profildaten bearbeiten und beispielsweise Kontaktdaten ändern oder ein Profilbild hinterlegen. Über die Funktion Passwort-Reset können sie eigenständig ihr Passwort zurücksetzen und ändern.

![Screenshot from the Nubus web portal in openDesk](../_media/Nubus-oD-Portal_DE.png)

# Identity Provider für Single Sign-on und Single-Logout sowie Zwei-Faktor-Authentisierung

Basis für den Zugriff über ein Single Sign-on, also die einmalige Anmeldung mit einem Benutzeraccount und einem Passwort, ist die Anbindung an einen Identity Provider. Das IAM von Univention ermöglicht grundsätzlich auch die Anbindung an externe Identity Provider.

Zur Abwehr von Cyberangriffen und Verhinderung einer missbräuchlichen Nutzung von Benutzerprofilen implementiert das IAM eine (Login) Brute Force Prevention. Erhöht wird die Sicherheit für das Login außerdem durch eine gruppenbasiert aktivierbare Zwei-Faktor-Authentisierung auf Basis von zeitbasierten Einmalpasswörtern (TOTP).

# Kubernetes basierte Cloud-Technologie

Die Komponenten von Univention für das IAM, die Integration der unterschiedlichen IT-Komponenten und das Portal, sind als containerisierte Kubernetes-Komponenten umgesetzt, so dass der Betrieb von openDesk sowohl im eigenen Rechenzentrum als auch als Clouddienst bei einem Hoster möglich ist.

Mehr Informationen zu den Open-Source-Lösungen für das Identitäts- und Zugriffsmanagement von Univention gibt es auf der Webseite [www.univention.de](https://www.univention.de).
