**_Die Deutsche Version finden Sie [hier](./Element_as_part_of_openDesk_DE.md)._**

# Element: Sovereign and Secure Communications for openDesk - What Does the Open Source Software Offer?

[Element](http://www.element.io/) provides real time messaging and collaboration, as well as voice and video calls (both 1:1s and conferencing). The end-to-end encrypted Element platform ensures digital sovereignty, whether it is self-hosted or hosted by a service provider. Element is designed to be as easy-to-use as consumer messaging apps, such as WhatsApp, but with the compliance that public sector organizations require. It also delivers the collaboration functionality and productivity associated with enterprise cloud apps like Slack and Microsoft Teams, but digitally sovereign and end-to-end encrypted.

## Areas of application for Element

[**Element**](http://www.element.io/) is a secure communications platform that is available under AGPL v3 (for the backend, called Synapse) and Apache 2.0 (for the front end client, called Element Web). It is available from Open CoDE at [a hosted GitLab instance](https://gitlab.opencode.de/bmi/opendesk/component-code/realtimecommunication/element), and is available as [Element Starter](https://ems.element.io/server-registration/self-host/trial) from the Element website.

The Element platform is available as part of openDesk for self-installation and self-hosting, free of charge. This includes messaging, collaboration and video calling using openDesk’s implementation of Jitsi.

Beyond openDesk, Element also offers extra support and features controlled through an extended [Admin Console](https://element.io/enterprise-functionality/admin-console), giving admins the power to easily manage an organization-wide deployment. It includes advanced identity management, auditing, moderation and data retention options as well as Long Term Support (LTS) and Service Level Agreements (SLAs). The [Element Server Suite](https://element.io/server-suite) (ESS) can be used to support any Matrix-based front end client.

Element is built on the [decentralized Matrix open standard](https://matrix.org/) for real time communications. Indeed Element is led by the same team that created the Matrix open standard, and is equally committed to open source.

Being Matrix-based means that Element is natively interoperable with any other Matrix-based solution; similar to how SMTP enables various email solutions to interoperate. Operating on an open standard ensures that multiple parties can communicate securely, and in real time, with each maintaining its own data sovereignty. For openDesk, this means that all organizations in the public sector that use openDesk - or any other Element deployment or third-party Matrix software stack - can communicate with each other (provided administrators have not deliberately limited access). Across any such federation, each organization still keeps ownership and control of its own data.

Element’s server-side functionality can support any Matrix-based app. Element Server Suite is also used in other large deployments such as [TI-Messenger](https://element.io/solutions/ti-messenger-gematik-matrix) in the German health sector, as specified by Gematik. Element also offers [Secure Border Gateways](https://element.io/server-suite/secure-border-gateways) and [Cross Domain Gateways](https://element.io/server-suite/cross-domain-solutions) to support Matrix-based federations, making it possible to connect high-side and low-side environments.

At openDesk, Element works closely with the Federal Ministry of the Interior and Home Affairs (BMI), the Centre for Digital Sovereignty (ZenDiS) and other players in the private sector open source community.. Customers include BWI for the Bundeswehr’s BwMessenger, Gematik for TI-Messenger and LOGINEO for schools in North Rhine-Westphalia. Outside of Germany, Element also works with DINUM for the French government’s Tchap messenger, NATO, the United Nations and the US Department of Defense.

## The use cases of Element are:

- Digitally sovereign messenger (as an alternative to consumer-grade messengers, such as WhatsApp and Signal)
- Digitally sovereign collaboration (as an alternative to MS Teams and Slack), with the additional benefit of end-to-end encryption and self-hosting
- Real time communications with organization-wide advanced identity and access management, including Single Sign-On (SSO)
- A communications platform that supports Germany’s public sector compliance requirements, including sovereign hosting, retention, auditing and moderation
- A fully end-to-end encrypted video platform, Element Call, that in the future will replace the current Jitsi video stack for improved digital sovereignty

![Screenshot from the Element application](../_media/element-de.png)

## Highlight features of Element

- **Digital sovereignty:** Element can be self-hosted, to ensure complete data ownership and control
- **Messaging:** Send secure messages with files, videos, images, audio
- **Collaboration:** Improve team productivity with a variety of add-ons from Matrix-based vendors including Element and Nordeck
- **VoIP:** From 1:1 and groups, through to large scale conferencing
- **Widgets:** Applications can run directly inside Element rooms and benefit from the underlying Matrix encryption, e.g. the widgets of Nordeck, which are also integrated in OpenDesk.
- **Federation:** Decentralization across servers including the use of Secure Border Gateways to control access between different security levels
- **User access control:** Use identity management and Spaces to create and manage organization-wide room hierarchies and permissions
- **Compliance:** Ensure compliance through a range of tools including Advanced Identity Management, retention, auditing and moderation

If you would like to find out more about how Element works and learn more about the features and use cases, visit [element.io](http://www.element.io/).