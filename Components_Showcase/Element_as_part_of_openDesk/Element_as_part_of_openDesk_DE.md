**_Please find the English version [here](./Element_as_part_of_openDesk_EN.md)_.**

# Element: Souveräne und sichere Kommunikation für openDesk - Was bietet die Open Source Software?

[Element](http://www.element.io/)  ermöglicht Messaging und Zusammenarbeit in Echtzeit sowie Sprach- und Videoanrufe (sowohl 1:1 als auch Konferenzen). Die Ende-zu-Ende-verschlüsselte Element-Plattform gewährleistet digitale Souveränität, unabhängig davon, ob sie selbst gehostet oder von einem Dienstanbieter betrieben wird. Element ist so benutzerfreundlich wie Messaging-Apps für Privatanwender, z. B. WhatsApp, und erfüllt gleichzeitig die Compliance-Anforderungen, die Organisationen des öffentlichen Sektors stellen. Es bietet auch die Funktionalität und Produktivität der Zusammenarbeit, die mit Cloud-Apps für Unternehmen wie Slack und Microsoft Teams verbunden sind, jedoch mit digitaler Souveränität und Ende-zu-Ende-Verschlüsselung.

## Anwendungsbereiche für Element

[Element](http://www.element.io/) ist eine sichere Kommunikationsplattform, die unter anerkannter Open-Source-Lizenz frei verfügbar ist. Der Quellcode kann von der [Open-Source-Plattform des Bundes Open CoDE](https://gitlab.opencode.de/bmi/opendesk/component-code/realtimecommunication/element) heruntergeladen werden, ist aber auch als [Element Starter](https://ems.element.io/server-registration/self-host/trial) auf der Element-Website erhältlich.

Die Element-Plattform ist als Teil von openDesk für die Selbstinstallation und das Selbst-Hosting kostenlos verfügbar. Dies beinhaltet Messaging, Zusammenarbeit und Videoanrufe unter Verwendung der openDesk-Implementierung von Jitsi.

Über openDesk hinaus bietet Element zusätzlichen Support und Funktionen, die über eine erweiterte [Admin Console](https://element.io/enterprise-functionality/admin-console) gesteuert werden und Administratoren die Möglichkeit geben, eine unternehmensweite Bereitstellung einfach zu verwalten. Dazu gehören erweiterte Optionen für Identitätsmanagement, Auditing, Moderation und Datenspeicherung sowie Long Term Support (LTS) und Service Level Agreements (SLAs).

Element basiert auf dem [dezentralen offenen Matrix-Standard](https://matrix.org/) für Echtzeitkommunikation. Die Firma wird von demselben Team geleitet, das den offenen Matrix-Standard entwickelt hat und kann daher die tiefe Expertise für den produktiven und professionellen Einsatz an ihre Kunden weitergeben.

Da Element auf Matrix basiert, ist es von Haus aus mit jeder anderen Matrix-basierten Lösung interoperabel, ähnlich wie SMTP die Interoperabilität verschiedener E-Mail-Lösungen ermöglicht. Die Verwendung eines offenen Standards stellt sicher, dass mehrere Parteien sicher und in Echtzeit kommunizieren können, wobei jede ihre eigene Datenhoheit behält. Für openDesk bedeutet dies, dass alle Organisationen im öffentlichen Sektor, die openDesk - oder eine andere Element-Implementierung oder ein Matrix-Softwarepaket eines Drittanbieters - verwenden, miteinander kommunizieren können (vorausgesetzt, die Administratoren haben den Zugang nicht absichtlich eingeschränkt). Bei einer solchen Konstellation behält jede Organisation weiterhin das Eigentum und die Kontrolle über ihre eigenen Daten.

Die serverseitige Funktionalität von Element, genannt [Element Server Suite](https://element.io/server-suite/cross-domain-solutions), kann jede Matrix-basierte Anwendung unterstützen. Die Element Server Suite wird auch in anderen großen Implementierungen wie dem [TI-Messenger](https://element.io/solutions/ti-messenger-gematik-matrix) im deutschen Gesundheitswesen eingesetzt, wie von Gematik spezifiziert. Element bietet auch [Secure Border Gateways](https://element.io/server-suite/secure-border-gateways) und [Cross Domain Gateways](https://element.io/server-suite/cross-domain-solutions) zur Unterstützung von Matrix-basierten Anwendungen an, die es ermöglichen, High-Side- und Low-Side-Umgebungen mit unterschiedlichen Geheimhaltungsstufen zu verbinden.

Element arbeitet bei openDesk eng mit dem Bundesministerium des Innern und für Heimat (BMI), dem Zentrum für Digitale Souveränität (ZenDiS) sowie  weiteren Akteuren der privatwirtschaftlichen Open-Source-Community zusammen.

Zu den Kunden gehören die BWI für den BwMessenger der Bundeswehr, die Gematik für den TI-Messenger und LOGINEO für die Schulen in Nordrhein-Westfalen. Außerhalb Deutschlands arbeitet Element auch mit DINUM für den Tchap-Messenger der französischen Regierung, der NATO, den Vereinten Nationen und dem US-Verteidigungsministerium zusammen.

## Die Anwendungsfälle von Element sind:

- Digitaler souveräner Messenger (als Alternative zu Consumer-Messenger-Angeboten wie WhatsApp oder Signal)
- Digital souveräne Zusammenarbeit (als Alternative zu MS Teams und Slack), mit dem zusätzlichen Vorteil der Ende-zu-Ende-Verschlüsselung und der Option zum eigenen Hosting oder Nutzung einer private Cloud
- Echtzeitkommunikation mit unternehmensweitem fortschrittlichem Identitäts- und Zugriffsmanagement, einschließlich Single Sign-On (SSO)
- Eine Kommunikationsplattform, die die Compliance-Anforderungen des öffentlichen Sektors in Deutschland gewährleistet, einschließlich souveränes Hosting, Aufbewahrung, Prüfung und Moderation
- Eine vollständig Ende-zu-Ende-verschlüsselte Videoplattform, Element Call, die in Zukunft den aktuellen Jitsi-Videostack für eine verbesserte digitale Souveränität und erhöhte Sicherheit ersetzen wird

![Screenshot aus der Element-Anwendung](../_media/element-de.png)

## Besondere Merkmale von Element

- **Digitale Souveränität:** Element kann selbst gehostet werden, um den vollständigen Besitz und die Kontrolle der Daten zu gewährleisten
- **Nachrichtenübermittlung:** Versenden Sie sichere Nachrichten mit Dateien, Videos, Bildern, Audio
- **Kollaboration:** Verbessern Sie die Produktivität mit einer Vielzahl von Add-ons von Matrix-basierten Anbietern wie Element und Nordeck
- **VoIP:** Von 1:1 über Gruppen bis hin zu groß angelegten Konferenzen
- **Widgets:** Fachverfahren und Anwendungen laufen direkt in Element Räumen und profitieren von der zu Grunde liegenden Matrix Verschlüsselung, z.B. die Widgets der Firma Nordeck, die ebenfalls in OpenDesk integriert sind.
- **Zusammenschluss:** Dezentralisierung über Server hinweg, einschließlich der Verwendung von Secure Border Gateways zur Steuerung des Zugriffs zwischen verschiedenen Sicherheitsstufen
- **Benutzer-Zugriffskontrolle:** Nutzung von Identitätsmanagement und Spaces zur Erstellung und Verwaltung unternehmensweiter Raumhierarchien und Berechtigungen
- **Einhaltung von Vorschriften:** Sicherstellung der Compliance durch eine Reihe von Tools, einschließlich Advanced Identity Management, Aufbewahrung, Auditing und Moderation

Wenn Sie mehr über die Funktionsweise von Element erfahren möchten und mehr über die Funktionen und Anwendungsfälle lernen wollen, besuchen Sie [element.io](http://www.element.io/).