# The office and collaboration suite for the public administration

Work together with colleagues on a document from anywhere without constantly sending emails back and forth. Spontaneously coordinate open questions in a video conference and come to a decision quickly. Efficiently manage projects across teams and departments or make the knowledge of individuals accessible to everyone.

Whether working remotely at home or from your desk in the office: the Office & Collaboration Suite openDesk opens up these and a thousand other possibilities for employees in offices and authorities to redesign their work and collaboration and manage processes more efficiently.

## Three good reasons for openDesk

### 1. Collaboration

openDesk enables effective digital collaboration in the public administration: creating projects, working together on documents, wiki, chat, video conferencing and much more - all from a single source.

### 2. Oriented to the needs of the public administration

openDesk is a demand-oriented solution for digital collaboration in the public administration: The software has been tailored to these specific requirements and needs.

### 3. Sovereignty and security

As open source software, openDesk strengthens digital sovereignty through full transparency and control over the source code. This openness also ensures a high level of security, as security gaps can be quickly identified and remedied. Data and processes are therefore reliably protected.

## Integration of powerful applications

openDesk integrates a range of powerful open source applications under one interface. This opens up numerous application possibilities for collaborative work in public administration.

An overview of all applications is shown in the following overview.

![openDesk image](./Images/openDesk_Komponenten_EN.png)

Our [OVERVIEW](./OVERVIEW.md) provides further (technical) insights (currently German only).

## Feedback and participation

openDesk is being continuously developed, which is why we welcome suggestions for improvement and feedback.
The feedback channels can be found [here in the "Participation and Involvement" section](https://gitlab.opencode.de/bmi/opendesk/info/-/blob/main/OVERVIEW.md?ref_type=heads#r%C3%BCckmeldungen-und-beteiligung).