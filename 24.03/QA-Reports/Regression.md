<!--
SPDX-FileCopyrightText: 2024 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
-->

<h1>QA Report - Regression test</h1>

* [Identified deviations](#identified-deviations)
* [Test set: Regression](#test-set-regression)
  * [Overview](#overview)
  * [Test cases](#test-cases)

# Identified deviations

| ID      | Priority | Result  | Details                                                                                                                                                                                                                                                                              |
| ------- | -------- | ------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| RT-0134 | High     | failed  | The diagrams.net editor does not support a "read only" mode, therefore there is currently no option to properly share a diagram file in Nextcloud using the "Display only" option. See [openDesk issue #37](https://gitlab.opencode.de/bmi/opendesk/deployment/opendesk/-/issues/37) |
| RT-0152 | High     | failed  | Inserting an image from Nextcloud did not work (nothing happend when selecting the option). This was fixed by a configurational change in [v0.5.79](https://gitlab.opencode.de/bmi/opendesk/deployment/opendesk/-/releases/v0.5.79).                                                 |
| RT-0184 | High     | failed  | Single-Logout from XWiki does not work as expected. This is a known issue as XWiki does not yet support OIDC Backchannel or Frontchannel Logout flows. See [openDesk issue #38](https://gitlab.opencode.de/bmi/opendesk/deployment/opendesk/-/issues/38)                             |
| RT-0119 | Normal   | failed  | Rendering of the submenu detailling the contact options in the right upper contact search dropdown is cut off. This was fixed with the NC28 upgrade as part of [v0.5.77](https://gitlab.opencode.de/bmi/opendesk/deployment/opendesk/-/releases/v0.5.77).                            |
| RT-0103 | High     | failed  | Two agenda item features from the OpenProject meeting module went missing. See [openDesk Issue #40](https://gitlab.opencode.de/bmi/opendesk/deployment/opendesk/-/issues/40)                                                                                                         |
| RT-0172 | High     | skipped | Function not available in openDesk. Currently there is no admin role for XWiki supported in openDesk. See [openDesk issue #37](https://gitlab.opencode.de/bmi/opendesk/deployment/opendesk/-/issues/37)                                                                              |
| RT-0173 | High     | skipped | see RT-0172                                                                                                                                                                                                                                                                          |
| RT-0016 | Normal   | skipped | Function not (yet) available in openDesk. Currently there is not admin role für Open-Xchange planned.                                                                                                                                                                                |
| RT-0017 | Normal   | skipped | See RT-0017                                                                                                                                                                                                                                                                          |
| RT-0018 | Normal   | skipped | See RT-0017                                                                                                                                                                                                                                                                          |

```mermaid
%%{init: {'themeVariables': { 'pie1': '#5e27dd', 'pie2': '#0c3ff3', 'pie3': '#ff529e', 'pie4': '#00ffcd', 'pie5': '#ffc700', 'pie6': '#52c1ff', 'pie7': '#adb3bc'}}}%%
pie showData
    title Deviation distribution
    "Successful": 176
    "Skipped cases": 6
    "Failedd test cases": 4
```

# Test set: Regression

## Overview

```mermaid
%%{init: {'themeVariables': { 'pie1': '#5e27dd', 'pie2': '#0c3ff3', 'pie3': '#ff529e', 'pie4': '#00ffcd', 'pie5': '#ffc700', 'pie6': '#52c1ff', 'pie7': '#adb3bc'}}}%%
pie showData
    title Test cases by component
    "Live communication - Chat & collaboration": 29
    "Live communication - Ad hoc videoconference": 35
    "Communication": 32
    "Projects": 14
    "Portal": 6
    "Productivity": 50
    "Knowledge": 20
```

```mermaid
%%{init: {'themeVariables': { 'pie1': '#5e27dd', 'pie2': '#0c3ff3', 'pie3': '#ff529e', 'pie4': '#00ffcd', 'pie5': '#ffc700', 'pie6': '#52c1ff', 'pie7': '#adb3bc'}}}%%
pie showData
    title Test cases by priority
    "High":143
    "Low":9
    "Normal":34
```

## Test cases

| ID      | Category                                    | Priority | Content                                                                                                                                            |
| ------- | ------------------------------------------- | -------- | -------------------------------------------------------------------------------------------------------------------------------------------------- |
| RT-0001 | Live communication - Chat & collaboration   | High     | Series appointment - Create a new repeated meeting                                                                                                 |
| RT-0002 | Live communication - Chat & collaboration   | High     | Series appointment - Creating a repeated meeting from an single appointment                                                                        |
| RT-0003 | Live communication - Chat & collaboration   | High     | Series appointment - Mandatory data of a repeated meeting                                                                                          |
| RT-0004 | Live communication - Chat & collaboration   | High     | Series appointment - Possibility of maintaining a repeated meeting                                                                                 |
| RT-0005 | Live communication - Chat & collaboration   | High     | Series appointment - Extend or shorten a repeated meeting                                                                                          |
| RT-0006 | Live communication - Chat & collaboration   | High     | Series appointment - Editing the content of an entire series                                                                                       |
| RT-0007 | Live communication - Chat & collaboration   | High     | Series appointment - Editing the content of a single appointment in a series                                                                       |
| RT-0008 | Live communication - Chat & collaboration   | High     | Series appointment - All information from the series appointment is retained                                                                       |
| RT-0009 | Live communication - Chat & collaboration   | High     | Lobby - Activation of “Ask to join” when creating a room by the creator                                                                            |
| RT-0010 | Live communication - Chat & collaboration   | High     | Check if screen sharing works in a private instance                                                                                                |
| RT-0011 | Live communication - Chat & collaboration   | High     | Incomplete creation of an appointment                                                                                                              |
| RT-0012 | Live communication - Chat & collaboration   | High     | For multiple breakout sessions (BS): Distribute all participants manually to BS                                                                    |
| RT-0013 | Live communication - Chat & collaboration   | High     | The moderator can send a message to all breakout sessions at the same time (e.g. "You still have 3 minutes")                                       |
| RT-0014 | Live communication - Chat & collaboration   | High     | Check whether ext. Guests cannot create appointments themselves after a ban (known bug in the preview system)                                      |
| RT-0015 | Live communication - Chat & collaboration   | Low      | Display title, description, date, time                                                                                                             |
| RT-0016 | Live communication - Chat & collaboration   | Normal   | Authorization for guest editing by the moderator                                                                                                   |
| RT-0017 | Live communication - Chat & collaboration   | Normal   | Integration of a digital notepad as a widget                                                                                                       |
| RT-0018 | Live communication - Chat & collaboration   | Normal   | Integration of image and PDF files into the whiteboard                                                                                             |
| RT-0019 | Live communication - Chat & collaboration   | High     | Test: Simultaneous image transmissions                                                                                                             |
| RT-0020 | Live communication - Chat & collaboration   | High     | External communication (job interviews, etc.)                                                                                                      |
| RT-0021 | Live communication - Chat & collaboration   | High     | Access via VPN                                                                                                                                     |
| RT-0022 | Live communication - Chat & collaboration   | High     | Access via an internet browser                                                                                                                     |
| RT-0023 | Live communication - Chat & collaboration   | High     | Inviting a participant via an invalid email address                                                                                                |
| RT-0024 | Live communication - Chat & collaboration   | High     | Widgets: Video conference raise hand                                                                                                               |
| RT-0025 | Live communication - Chat & collaboration   | High     | Chat - Have a conversation                                                                                                                         |
| RT-0026 | Live communication - Chat & collaboration   | High     | Chat - Verify proper logout from the chat application                                                                                              |
| RT-0027 | Live communication - Chat & collaboration   | Normal   | Chat: Send files in chat                                                                                                                           |
| RT-0028 | Live communication - Chat & collaboration   | High     | Widgets: Call up the Whiteboard widget                                                                                                             |
| RT-0029 | Live communication - Chat & collaboration   | Low      | Meeting planner: Delete meetings                                                                                                                   |
| RT-0030 | Live communication - Ad hoc videoconference | Normal   | Ad hoc videoconference - Disable all cameras                                                                                                       |
| RT-0031 | Live communication - Ad hoc videoconference | Normal   | Ad hoc videoconference - Mute all participants                                                                                                     |
| RT-0032 | Live communication - Ad hoc videoconference | Normal   | Ad hoc videoconference - Enter a breakout room as a moderator                                                                                      |
| RT-0033 | Live communication - Ad hoc videoconference | Normal   | Ad hoc videoconference - Enter a breakout room as a participant                                                                                    |
| RT-0034 | Live communication - Ad hoc videoconference | Normal   | Ad hoc videoconference - Show participants                                                                                                         |
| RT-0035 | Live communication - Ad hoc videoconference | High     | Ad hoc videoconference - Entering a room by the moderator                                                                                          |
| RT-0036 | Live communication - Ad hoc videoconference | High     | Ad hoc videoconference - Entering a room by a participant after the moderator                                                                      |
| RT-0037 | Live communication - Ad hoc videoconference | High     | Ad hoc videoconference - A participant enters a room before the moderator                                                                          |
| RT-0038 | Live communication - Ad hoc videoconference | High     | Ad hoc videoconference - Share screen                                                                                                              |
| RT-0039 | Live communication - Ad hoc videoconference | Normal   | Ad hoc videoconference - Cancel screen sharing                                                                                                     |
| RT-0040 | Live communication - Ad hoc videoconference | Normal   | Ad hoc videoconference - Add breakout room                                                                                                         |
| RT-0041 | Live communication - Ad hoc videoconference | Normal   | Ad hoc videoconference - Delete breakout room                                                                                                      |
| RT-0042 | Live communication - Ad hoc videoconference | High     | Ad hoc videoconference - Open chat and post a message in the chat                                                                                  |
| RT-0043 | Live communication - Ad hoc videoconference | Low      | Ad hoc videoconference - Close a Chat                                                                                                              |
| RT-0044 | Live communication - Ad hoc videoconference | High     | Ad hoc videoconference - Create a new room                                                                                                         |
| RT-0045 | Live communication - Ad hoc videoconference | High     | Ad hoc videoconference - Explicitly assuming the role of moderator                                                                                 |
| RT-0046 | Live communication - Ad hoc videoconference | Normal   | Ad hoc videoconference - Take the raised hand back                                                                                                 |
| RT-0047 | Live communication - Ad hoc videoconference | Normal   | Ad hoc videoconference - Hand raising in a video conference                                                                                        |
| RT-0048 | Live communication - Ad hoc videoconference | Low      | Ad hoc videoconference - Set background                                                                                                            |
| RT-0049 | Live communication - Ad hoc videoconference | Normal   | Ad hoc videoconference - Activate tile view                                                                                                        |
| RT-0050 | Live communication - Ad hoc videoconference | High     | Ad hoc videoconference - Activate camera and transmit image                                                                                        |
| RT-0051 | Live communication - Ad hoc videoconference | Normal   | Ad hoc videoconference - Deactivate camera                                                                                                         |
| RT-0052 | Live communication - Ad hoc videoconference | Normal   | Ad hoc videoconference - Avtivate Lobby                                                                                                            |
| RT-0053 | Live communication - Ad hoc videoconference | High     | Ad hoc videoconference - Activate microphone                                                                                                       |
| RT-0054 | Live communication - Ad hoc videoconference | Normal   | Ad hoc videoconference - Deactivate microphone                                                                                                     |
| RT-0055 | Live communication - Ad hoc videoconference | Normal   | Ad hoc videoconference - Mute a participant's microphone                                                                                           |
| RT-0056 | Live communication - Ad hoc videoconference | High     | Ad hoc videoconference - Set password for a video conference                                                                                       |
| RT-0057 | Live communication - Ad hoc videoconference | High     | Ad hoc videoconference - Invite people before starting a video converence                                                                          |
| RT-0058 | Live communication - Ad hoc videoconference | High     | Ad hoc videoconference - Participants enter the video conference via the lobby                                                                     |
| RT-0059 | Live communication - Ad hoc videoconference | High     | Ad hoc videoconference - Reject participants in lobby                                                                                              |
| RT-0060 | Live communication - Ad hoc videoconference | Low      | Ad hoc videoconference - Run survey                                                                                                                |
| RT-0061 | Live communication - Ad hoc videoconference | Low      | Ad hoc videoconference - Create survey                                                                                                             |
| RT-0062 | Live communication - Ad hoc videoconference | Normal   | Ad hoc videoconference - Reset video conference from full screen mode                                                                              |
| RT-0063 | Live communication - Ad hoc videoconference | Normal   | Ad hoc videoconference - Set video conference to full screen mode                                                                                  |
| RT-0064 | Live communication - Ad hoc videoconference | High     | Ad hoc videoconference - Access for participants via a password                                                                                    |
| RT-0065 | Communication                               | High     | Termin - Appointment with video conference - entry by an invited participan                                                                        |
| RT-0066 | Communication                               | High     | Hauptmenü - Call subcomponent video conferences                                                                                                    |
| RT-0067 | Communication                               | High     | Email - Write an email with a file as a file picker attachment and send it internally                                                              |
| RT-0068 | Communication                               | High     | Email - Write an email with a file link as a file picker attachment and send it internally                                                         |
| RT-0069 | Communication                               | High     | Termin - Create an appointment with video conference                                                                                               |
| RT-0070 | Communication                               | High     | Termin - Appointment with video conference - entry by the organizer                                                                                |
| RT-0071 | Communication                               | High     | Lobby - Activation of “Ask to join” in an existing room by the creator                                                                             |
| RT-0072 | Communication                               | High     | Lobby - Notification of a waiting guest to the creator of the room                                                                                 |
| RT-0073 | Communication                               | High     | Lobby - Confirm waiting guest by room creator                                                                                                      |
| RT-0074 | Communication                               | High     | Lobby - Reject waiting guest by room creator                                                                                                       |
| RT-0075 | Communication                               | High     | Lobby - Request participation in a room as a guest                                                                                                 |
| RT-0076 | Communication                               | High     | Lobby - As a guest, receive a notification that the creator is waiting for confirmation                                                            |
| RT-0077 | Communication                               | High     | Lobby - Receive notification as a guest > participation accepted                                                                                   |
| RT-0078 | Communication                               | High     | Lobby - Received notification as a guest > participation refused                                                                                   |
| RT-0079 | Communication                               | High     | Lobby - Participation as a guest in a rejected room is not possible                                                                                |
| RT-0080 | Communication                               | High     | Look and Feel - Horizontal menu area visible at the top of the chat and collaboration application                                                  |
| RT-0081 | Communication                               | High     | Look and Feel - No display of the icons on the right side of the menu area                                                                         |
| RT-0082 | Communication                               | High     | Look and Feel - Checking the color values ​​of the menu area                                                                                       |
| RT-0083 | Communication                               | High     | Look and Feel - 9-point menu function                                                                                                              |
| RT-0084 | Communication                               | High     | Rights and role concept - checks permissions admin                                                                                                 |
| RT-0085 | Communication                               | High     | Rights and role concept - checks permissions user                                                                                                  |
| RT-0086 | Communication                               | High     | Translation of all Jitsi widgets into German                                                                                                       |
| RT-0087 | Communication                               | High     | Welcome message upon first use                                                                                                                     |
| RT-0088 | Communication                               | High     | Widget Toggle - Widgets can be hidden and shown                                                                                                    |
| RT-0089 | Communication                               | Normal   | Set a follow-up appointment for a meeting                                                                                                          |
| RT-0090 | Communication                               | Normal   | Contacts: Administration                                                                                                                           |
| RT-0091 | Communication                               | Normal   | Distribution list: administration                                                                                                                  |
| RT-0092 | Communication                               | Normal   | Groupware solution has mail, calendar, contacts, tasks                                                                                             |
| RT-0093 | Communication                               | Normal   | Multiple emails opened at the same time                                                                                                            |
| RT-0094 | Communication                               | High     | Groupware - create and send message                                                                                                                |
| RT-0095 | Communication                               | High     | Create meeting - with additional participants - external - not in the global address book                                                          |
| RT-0096 | Communication                               | High     | Create an appointment with an attachment from the host system                                                                                      |
| RT-0097 | Projects                                    | High     | Projects - Component is made available to a new user                                                                                               |
| RT-0098 | Projects                                    | High     | Projects - Deletions of work packages in the projects calendar are also visible in Communicate & organize - Calendar after the sync run            |
| RT-0099 | Projects                                    | High     | Projects - Provide project calendar in iCal format                                                                                                 |
| RT-0100 | Projects                                    | High     | Projects - Access authorization via iCal token                                                                                                     |
| RT-0101 | Projects                                    | High     | Projects - Subscribing to a projects calendar in the Communicate & organize - Calendar                                                             |
| RT-0102 | Projects                                    | High     | Projects - Subscribed projects calendar are visible in the Communicate & organize - Calendar next to the initial Communicate & organize - Calendar |
| RT-0103 | Projects                                    | High     | Projects - Agendas and minutes - Specify involved people                                                                                           |
| RT-0104 | Projects                                    | High     | Projects - Agendas and minutes - Determination of the duration of the agenda item                                                                  |
| RT-0105 | Projects                                    | High     | Projects - Agendas and minutes - Provide reference to work package                                                                                 |
| RT-0106 | Projects                                    | High     | Projects - Agendas and minutes - Determine the typification of the agenda item                                                                     |
| RT-0107 | Projects                                    | High     | Projects - Changes in work packages in projects Calendars are also visible in the Communicate & organize - Calendar after the sync run             |
| RT-0108 | Projects                                    | High     | Projects - GUI for creating a project calendar in iCal format                                                                                      |
| RT-0109 | Projects                                    | High     | Projects - Component is available and can be called                                                                                                |
| RT-0110 | Projects                                    | High     | Projects - Component has all required functions                                                                                                    |
| RT-0111 | Portal                                      | High     | Tiles of the homepage after successful login                                                                                                       |
| RT-0112 | Portal                                      | High     | Dashboard - Shortcut component Fileshare - Files                                                                                                   |
| RT-0113 | Portal                                      | High     | Dashboard - Direct call component Groupware - Calendar                                                                                             |
| RT-0114 | Portal                                      | High     | Cross-module single sign-on                                                                                                                        |
| RT-0115 | Portal                                      | Normal   | User self-service: Change user data                                                                                                                |
| RT-0116 | Portal                                      | Normal   | User self-service: password change                                                                                                                 |
| RT-0117 | Productivity                                | High     | Main menu - call subcomponent Email                                                                                                                |
| RT-0118 | Productivity                                | High     | Main menu - call subcomponent tasks                                                                                                                |
| RT-0119 | Productivity                                | Normal   | Contacts - direct call to create an email                                                                                                          |
| RT-0120 | Productivity                                | High     | create folder                                                                                                                                      |
| RT-0121 | Productivity                                | High     | Web Office components include word processing, spreadsheet and presentation programs                                                               |
| RT-0122 | Productivity                                | High     | Files - Download a document                                                                                                                        |
| RT-0123 | Productivity                                | High     | Files - Calling the component                                                                                                                      |
| RT-0124 | Productivity                                | High     | Files - Upload a document                                                                                                                          |
| RT-0125 | Productivity                                | High     | Diagrams - Embedding images from other platforms is possible                                                                                       |
| RT-0126 | Productivity                                | Normal   | Diagrams - Collaborate on a slide                                                                                                                  |
| RT-0127 | Productivity                                | High     | Diagrams - Ability for a single user to open and edit Diagrams                                                                                     |
| RT-0128 | Productivity                                | High     | Diagrams - Open the file directly from files by clicking                                                                                           |
| RT-0129 | Productivity                                | High     | Diagrams - Import images from files directly into the Diagram Module                                                                               |
| RT-0130 | Productivity                                | High     | Diagrams - Open an empty Diagram file from files                                                                                                   |
| RT-0131 | Productivity                                | Low      | Files - Delete a document                                                                                                                          |
| RT-0132 | Productivity                                | Normal   | Diagrams - After closing a Diagram file, return to files                                                                                           |
| RT-0133 | Productivity                                | Low      | Files - Recovering a deleted document from the Recycle Bin                                                                                         |
| RT-0134 | Productivity                                | High     | Diagrams - Permission change on the host platform (when linking the file)                                                                          |
| RT-0135 | Productivity                                | Low      | Files - Deleting documents from the Recycle Bin                                                                                                    |
| RT-0136 | Productivity                                | High     | Sheet - Open sample file "conditional-format-icon.xlsx" in Excel and Sheet - formatting check                                                      |
| RT-0137 | Productivity                                | High     | Sheet - Open sample file "conditional-format-matrix.xlsx" in Excel and Sheet - formatting check                                                    |
| RT-0138 | Productivity                                | High     | Sheet - Opening a pivot table created in Sheet in Excel                                                                                            |
| RT-0139 | Productivity                                | High     | Sheet - Opening a pivot table created in Excel in Sheet                                                                                            |
| RT-0140 | Productivity                                | Normal   | Weboffice - Parallel editing of a document - Case 4: Editing and deleting a document at the same time                                              |
| RT-0141 | Productivity                                | High     | Weboffice- Parallel editing of a document - Case 2: Simultaneous editing in the same place                                                         |
| RT-0142 | Productivity                                | High     | Weboffice - Creating a new document in Files                                                                                                       |
| RT-0143 | Productivity                                | High     | Weboffice - Parallel editing of a document - Case 1: Simultaneous editing in different places                                                      |
| RT-0144 | Productivity                                | Normal   | Weboffice - Parallel editing of a document - Case 3: Editing and moving a document at the same time                                                |
| RT-0145 | Productivity                                | High     | Weboffice - Adaptation to German MS Office keyboard shortcuts                                                                                      |
| RT-0146 | Productivity                                | High     | Weboffice - Check tooltip on sample document                                                                                                       |
| RT-0147 | Productivity                                | High     | Weboffice - Use of document themes                                                                                                                 |
| RT-0148 | Productivity                                | High     | Document - Change tracking is the same as in Word                                                                                                  |
| RT-0149 | Productivity                                | High     | Document - A different header/footer on the first page is adopted correctly                                                                        |
| RT-0150 | Productivity                                | High     | Document - Section breaks cause empty paragraphs                                                                                                   |
| RT-0151 | Productivity                                | High     | Document - Processing document with themes                                                                                                         |
| RT-0152 | Productivity                                | High     | Document - Free positioning of an image (with text wrapping) is supported                                                                          |
| RT-0153 | Productivity                                | High     | Document - A STYLEREF field is supported in the footer of the document                                                                             |
| RT-0154 | Productivity                                | High     | Document - A STYLEREF field is supported in the header of the document                                                                             |
| RT-0155 | Productivity                                | High     | Document - Multi page floating frames and tables                                                                                                   |
| RT-0156 | Productivity                                | High     | Document - Floating shape anchoring general improvements                                                                                           |
| RT-0157 | Productivity                                | High     | Document - Table content that runs across a page boundary is fully displyed                                                                        |
| RT-0158 | Productivity                                | High     | Document - Text in Frames that runs across a page boundary is fully displyed                                                                       |
| RT-0159 | Productivity                                | High     | Document - Check whether Word captures same number of changes like Document                                                                        |
| RT-0160 | Productivity                                | High     | Sheet - Conditional formatting with sufficient performance                                                                                         |
| RT-0161 | Productivity                                | High     | Sheet - Retain cell formatting in pivot tables after load or refresh                                                                               |
| RT-0162 | Productivity                                | High     | Sheet - Filter application in compact view                                                                                                         |
| RT-0163 | Productivity                                | High     | Sheet - Filter application in tab view                                                                                                             |
| RT-0164 | Productivity                                | Normal   | Sheet - same default color for the background in Sheet and Excel                                                                                   |
| RT-0165 | Productivity                                | High     | Sheet - correct layout for pivot tables                                                                                                            |
| RT-0166 | Productivity                                | High     | Sheet - only one hyperlink per cell                                                                                                                |
| RT-0167 | Knowledge                                   | High     | Knowledge - Authentication - Single Sign on                                                                                                        |
| RT-0168 | Knowledge                                   | High     | Knowledge - Passing through documents and content within openDesk                                                                                  |
| RT-0169 | Knowledge                                   | High     | Knowledge - Integration into other openDesk components via link                                                                                    |
| RT-0170 | Knowledge                                   | High     | Knowledge - Integration into external artifacts                                                                                                    |
| RT-0171 | Knowledge                                   | High     | Knowledge - Component is available as a selectable component                                                                                       |
| RT-0172 | Knowledge                                   | High     | Knowledge - Deletion of a changed page by the owner                                                                                                |
| RT-0173 | Knowledge                                   | High     | Knowledge - Deletion of a page by the administrator                                                                                                |
| RT-0174 | Knowledge                                   | High     | Knowledge - Deletion of a page by the page owner                                                                                                   |
| RT-0175 | Knowledge                                   | High     | Knowledge - Deletion of a page by a third party                                                                                                    |
| RT-0176 | Knowledge                                   | High     | Knowledge - QuickActions - Call up the action menu                                                                                                 |
| RT-0177 | Knowledge                                   | High     | Knowledge - QuickActions - Basic editing functions                                                                                                 |
| RT-0178 | Knowledge                                   | High     | Knowledge - QuickActions - Quick action for uploading attachments                                                                                  |
| RT-0179 | Knowledge                                   | High     | Knowledge - QuickActions - Quick action for uploading images                                                                                       |
| RT-0180 | Knowledge                                   | High     | Knowledge - QuickActions - Quick action to add images using a keyboard shortcut                                                                    |
| RT-0181 | Knowledge                                   | High     | Knowledge - QuickActions - Quick action to add icons using a keyboard shortcut                                                                     |
| RT-0182 | Knowledge                                   | High     | Knowledge - QuickActions - Search and filter function in the actions menu                                                                          |
| RT-0183 | Knowledge                                   | High     | Knowledge - Usability - layout changes                                                                                                             |
| RT-0184 | Knowledge                                   | High     | Knowledge - Log out of the application                                                                                                             |
| RT-0185 | Knowledge                                   | High     | Knowledge - Changing pages                                                                                                                         |
| RT-0186 | Knowledge                                   | High     | Knowledge - Creating new pages                                                                                                                     |
