<!--
SPDX-FileCopyrightText: 2024 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
-->

<h1>QA Report - Smoke test</h1>

* [Identified deviations](#identified-deviations)
* [Test set: System](#test-set-system)
  * [Overview](#overview)
  * [Test cases](#test-cases)

# Identified deviations

No deviations identified.

# Test set: System

## Overview

```mermaid
%%{init: {'themeVariables': { 'pie1': '#5e27dd', 'pie2': '#0c3ff3', 'pie3': '#ff529e', 'pie4': '#00ffcd', 'pie5': '#ffc700', 'pie6': '#52c1ff', 'pie7': '#adb3bc'}}}%%
pie showData
    title Test cases by component
    "Live communication - Chat & collaboration": 22
    "Communication": 14
    "Portal": 14
    "Productivity": 16
```

```mermaid
%%{init: {'themeVariables': { 'pie1': '#5e27dd', 'pie2': '#0c3ff3', 'pie3': '#ff529e', 'pie4': '#00ffcd', 'pie5': '#ffc700', 'pie6': '#52c1ff', 'pie7': '#adb3bc'}}}%%
pie showData
    title Test cases by priority
    "High": 44
    "Low": 6
    "Normal": 16
```

## Test cases

| ID      | Category                                  | Priority | Content                                                                                         |
| ------- | ----------------------------------------- | -------- | ----------------------------------------------------------------------------------------------- |
| SM-0001 | Communication                             | High     | Open Ad Hoc Videoconference from cental navigation                                              |
| SM-0002 | Communication                             | High     | Create and send internal mail with file attachment using the filepicker                         |
| SM-0003 | Communication                             | High     | Create and send internal mail with file link using the filepicker                               |
| SM-0004 | Communication                             | High     | Create meeting with link to videoconference                                                     |
| SM-0005 | Communication                             | High     | Join videoconference as moderator/organizer                                                     |
| SM-0006 | Communication                             | High     | Join videoconference as invitee                                                                 |
| SM-0007 | Communication                             | Normal   | All groupware components are available (Mail, Calendar, Contacts, Tasks)                        |
| SM-0008 | Communication                             | Normal   | Create followup meeting to an existing meeting                                                  |
| SM-0009 | Communication                             | Normal   | Manage distribution list                                                                        |
| SM-0010 | Communication                             | Normal   | Manage contacts                                                                                 |
| SM-0011 | Communication                             | Normal   | Work with multiple (at least two) emails at the same time                                       |
| SM-0012 | Communication                             | High     | Create a meeting and add an external participant that is not already in the address book        |
| SM-0013 | Communication                             | High     | Create a meeting and add an external participant that is not already in the address book        |
| SM-0014 | Communication                             | High     | Create and send an email                                                                        |
| SM-0015 | Live communication - Chat & collaboration | Low      | Check meeting details (title, description, date, time)                                          |
| SM-0016 | Live communication - Chat & collaboration | Normal   | Use digital notepad widget                                                                      |
| SM-0017 | Live communication - Chat & collaboration | Normal   | Add images and PDF files to whiteboard                                                          |
| SM-0018 | Live communication - Chat & collaboration | Normal   | Provide permission to edit whiteboard to guests                                                 |
| SM-0019 | Live communication - Chat & collaboration | High     | Check video (camera / screenshare) is working for at least two users                            |
| SM-0020 | Live communication - Chat & collaboration | High     | Check if external (anonymous) participants can join meetings                                    |
| SM-0021 | Live communication - Chat & collaboration | High     | Check access using VPN                                                                          |
| SM-0022 | Live communication - Chat & collaboration | High     | Check access using various browsers                                                             |
| SM-0023 | Live communication - Chat & collaboration | High     | Check single sign-on is working                                                                 |
| SM-0024 | Live communication - Chat & collaboration | Low      | Delete meeting from meeting planner                                                             |
| SM-0025 | Live communication - Chat & collaboration | High     | Check that an (anonymous) participant cannot act in the system after being banned               |
| SM-0026 | Live communication - Chat & collaboration | High     | Moderator to send a message to all breakout sessions                                            |
| SM-0027 | Live communication - Chat & collaboration | High     | Moderator to distribute participants among existing breakout sessions                           |
| SM-0028 | Live communication - Chat & collaboration | High     | Check that mandatory fields for a meeting are enforced                                          |
| SM-0029 | Live communication - Chat & collaboration | High     | Check if screen sharing works in a private browser instance                                     |
| SM-0030 | Live communication - Chat & collaboration | High     | Check if the email address validation (of an invitee) ensures a proper mail address was entered |
| SM-0031 | Live communication - Chat & collaboration | High     | Open the whiteboard widget                                                                      |
| SM-0032 | Live communication - Chat & collaboration | High     | Raise hand in video conferencing                                                                |
| SM-0033 | Live communication - Chat & collaboration | High     | Check the logout from the application works as expected                                         |
| SM-0034 | Live communication - Chat & collaboration | High     | Chat with another (internal) user                                                               |
| SM-0035 | Live communication - Chat & collaboration | Normal   | Send files via chat                                                                             |
| SM-0036 | Live communication - Chat & collaboration | High     | Open the meeting planner                                                                        |
| SM-0037 | Portal                                    | High     | Open Groupware Calendar                                                                         |
| SM-0038 | Portal                                    | High     | Open Fileshare                                                                                  |
| SM-0039 | Portal                                    | High     | Verify the user account is working and seeing all applications                                  |
| SM-0040 | Portal                                    | High     | Open Knowledge management                                                                       |
| SM-0041 | Portal                                    | High     | Open Project management                                                                         |
| SM-0042 | Portal                                    | High     | Single sign-on is working with all components                                                   |
| SM-0043 | Portal                                    | Normal   | User changes password in self service                                                           |
| SM-0044 | Portal                                    | Normal   | User changes personal data in self service                                                      |
| SM-0045 | Portal                                    | High     | Login with valid and invalid credentials                                                        |
| SM-0046 | Portal                                    | High     | Create a new user                                                                               |
| SM-0047 | Portal                                    | High     | Send invitation mail to new user                                                                |
| SM-0048 | Portal                                    | Low      | Delete user from user list                                                                      |
| SM-0049 | Portal                                    | Normal   | Check the admin options are available (admin user)                                              |
| SM-0050 | Portal                                    | Normal   | Check the admin options are not available (standard user)                                       |
| SM-0051 | Productivity                              | High     | Open Groupware Calendar from central navigation                                                 |
| SM-0052 | Productivity                              | High     | Open Groupware Email from central navigation                                                    |
| SM-0053 | Productivity                              | Normal   | Create email from Nextcloud contacts list                                                       |
| SM-0054 | Productivity                              | High     | Share a document with a personal contact created in the Communication’s contact module          |
| SM-0055 | Productivity                              | High     | Create a new odt-document from within Nextcloud                                                 |
| SM-0056 | Productivity                              | High     | Edit odt-document with multiple users: Edit in different places of the document                 |
| SM-0057 | Productivity                              | Normal   | Edit odt-document with multiple users: Edit and move the underlying document                    |
| SM-0058 | Productivity                              | Normal   | Edit odt-document with multiple users: Edit and delete the underlying document                  |
| SM-0059 | Productivity                              | High     | Edit odt-document with multiple users: Edit in the same place                                   |
| SM-0060 | Productivity                              | High     | Open all relevant web office document types: odt, ods, opd, odd                                 |
| SM-0061 | Productivity                              | High     | Create folder                                                                                   |
| SM-0062 | Productivity                              | Low      | Delete document                                                                                 |
| SM-0063 | Productivity                              | Low      | Delete files from trash                                                                         |
| SM-0064 | Productivity                              | Low      | Recover file from trash                                                                         |
| SM-0065 | Productivity                              | High     | Upload a document                                                                               |
| SM-0066 | Productivity                              | High     | Download a document                                                                             |
