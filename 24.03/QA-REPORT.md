<!--
SPDX-FileCopyrightText: 2024 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
-->

<h1>QA Report</h1>

* [Scope](#scope)
  * [Terminology](#terminology)
* [Test reports](#test-reports)

# Scope

Provide an overview of the functional regression test execution outcome based on the openDesk Release 24.03 which based on release [v0.5.74](https://gitlab.opencode.de/bmi/opendesk/deployment/opendesk/-/releases/v0.5.74) of the deployment automation.

When new features are added to openDesk or changes and bug fixes are made, there is always the risk that these changes may have side effects on existing functionalities. The regression test focuses on existing functionalities to ensure they work as expected. It aims to demonstrate that changes and updates in openDesk do not have unintended consequences.

The document will **not** provide details about the actual test execution and the utilized infrastructure.

## Terminology

| Term | Description |
| ---- | ----------- |
| ID | Unique identifier to uniquely identify the test case. |
| Priority | Sets the importance of a test case:<br/>`Low`: The test case has no high impact on user experience.<br/>`Normal`: The test case has impact on user experience.<br/>`High`: The test case have a higher priority for reasons like security relevant or high impact on user experience.|
| Result | Show the result after executing a test case:<br/>`skipped`: The test case was skipped for reasons like permission problems<br/>`failed`:The execution of a test case was not successful |
| Details | Details why a test case was skipped or failed. |
| Category | The involved component of openDesk. |

# Test reports

Please find the test reports below.

**Note:** We are in a transition between test tracking systems, therefore the provided test case IDs might be subject to change with future releases. We currently also do not have any automated regression test infrastructure. The tests were executed manually.

- [System test](./QA-Reports/System.md)
- [Regression test](./QA-Reports/Regression.md)
- [Smoke test](./QA-Reports/Smoke.md)
