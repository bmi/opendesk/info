<!--
SPDX-FileCopyrightText: 2024 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
-->

<h1>openDesk 24.03 - Release Notes</h1>

* [Überblick](#überblick)
* [Upstream Komponenten](#upstream-komponenten)
  * [Übersicht der Versionsstände](#übersicht-der-versionsstände)
  * [openDesk Entwicklungen](#opendesk-entwicklungen)
* [Plattform-Entwicklung](#plattform-entwicklung)
  * [Deploymentautomatisierung v0.5.74](#deploymentautomatisierung-v0574)
* [Weitere Liefergegenstände](#weitere-liefergegenstände)
  * [Anwenderdokumentation](#anwenderdokumentation)
  * [Architekturkonzept](#architekturkonzept)
  * [Barrierefreiheit](#barrierefreiheit)
  * [Datenschutz](#datenschutz)
  * [Erprobungskonzept](#erprobungskonzept)
  * [IT-Grundschutz](#it-grundschutz)
  * [Lizenz-Compliance](#lizenz-compliance)
  * [Qualitätssicherung](#qualitätssicherung)
  * [Releasekonzept](#releasekonzept)
* [Fußnoten](#fußnoten)

# Überblick

Mit dem openDesk Release 24.03 wurde der Fokus im Vergleich zu Vorrelease von Features hin auf den Bereich Rahmenbedingungen verlagert. Dieses Dokument zeigt die Neuerungen auf, die seit [openDesk 23.12](../23.12/README.md) stattgefunden haben.

# Upstream Komponenten

## Übersicht der Versionsstände

Die folgende Tabelle liefert einen Gesamtüberblick über die Versionstände der in openDesk integrierten Fachkomponenten, welche auch unabhängig von openDesk weiterentwickelt werden. So kann openDesk auf den zunehmenden Funktionsumfang der jeweiligen Versionen zurückgreifen. Umfangreiche Dokumentationen zu den Produkten sind im Regefall auf den Herstellerseiten zu finden.

Nicht alle in den Upstream-Produkten beschriebenen Funktionen sind auch in openDesk aktiviert. Wenn ein prinziell vorhandenes Feature nicht in openDesk zu finden ist, kann dazu gerne [ein Ticket angelegt](https://gitlab.opencode.de/bmi/opendesk/info/-/issues/new) werden, um prüfen zu lassen, ob die Funktion bereitgestellt werden kann oder ob diese bewusst nicht aktiviert wird.

| Funktionale<br/>Komponente  | Komponenten<br/>Version                                                                                        | Upstream Dokumentation                                                                                                                    |
| --------------------------- | -------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------- |
| Collabora                   | [23.05.6.4.1](https://www.collaboraoffice.com/collabora-online-23-05-release-notes/)                           | [Für das aktuellste Release](https://help.collaboraoffice.com/)                                                                           |
| Cryptpad ft. diagrams.net   | [5.6.0](https://github.com/cryptpad/cryptpad/releases/tag/5.6.0)                                               | [Für das aktuellste Release](https://docs.cryptpad.org/en/)                                                                               |
| Element ft. Nordeck widgets | [1.11.52](https://github.com/element-hq/element-desktop/blob/develop/CHANGELOG.md#changes-in-11152-2023-12-19) | [Für das aktuellste Release](https://element.io/user-guide)                                                                               |
| Jitsi                       | [2.0.8922](https://github.com/jitsi/jitsi-meet/releases/tag/stable%2Fjitsi-meet_8922)                          | [Für das aktuellste Release](https://jitsi.github.io/handbook/docs/category/user-guide/)                                                  |
| Nextcloud                   | [27.1.5](https://nextcloud.com/de/changelog/#27-1-5)                                                           | [Nextcloud 27](https://docs.nextcloud.com/#nextcloud-27)                                                                                  |
| OpenProject                 | [13.1.1](https://www.openproject.org/docs/release-notes/13-1-1/)                                               | [Für das aktuellste Release](https://www.openproject.org/docs/user-guide/)                                                                |
| Open-Xchange                | [8.20](https://documentation.open-xchange.com/appsuite/releases/8.20/)                                         | Online-Dokumentation innerhalb des Produkts vorhanden; [Weitere Dokumentations-Resourcen](https://www.open-xchange.com/resources/oxpedia) |
| Nubus                       | Produkt Preview[^1]                                                                                            | [Univention Dokumentations-Website](https://docs.software-univention.de/n/en/index.html)                                                  |
| XWiki                       | [15.10.4](https://www.xwiki.org/xwiki/bin/view/Blog/XWiki15104Released)                                        | [Für das aktuellste Release](https://www.xwiki.org/xwiki/bin/view/Documentation)                                                          |

## openDesk Entwicklungen

Zusätzlich zu der Produktroadmap des jeweiligen Herstellers lässt auch openDesk die Funktionalität der Fachkomponenten erweitern. Zugehörige Entwicklungen sind im Regelfall Teil der Upstream-Produkte und sind für openDesk 24.03 [dem Changelog](./CHANGELOG.md) zu entnehmen.

# Plattform-Entwicklung

## Deploymentautomatisierung v0.5.74

openDesk 24.03 liegt die Version [v0.5.74](https://gitlab.opencode.de/bmi/opendesk/deployment/opendesk/-/releases/v0.5.74) der [openDesk Deploymentautomatisierung](https://gitlab.opencode.de/bmi/opendesk/deployment/opendesk) zugrunde.

Sämtliche, ggf. auch aktuellere, Versionen der Deploymentautomatisierung sind jeweils mit Changelog im [Bereich Releases des zugehörigen Projekts](https://gitlab.opencode.de/bmi/opendesk/deployment/opendesk/-/releases) zu finden.

In diesem Kapitel werden lediglich größere Anpassungen - im Vergleich zu dem für openDesk 23.12 relevanten technischen Release - aufgeführt:

- Der Univention Corporate Server Container-Monolith wurde gegen den Univention Nubus Produkt-Preview[^1] ausgetauscht, der die relevanten IAM-Services in separaten Containern ausführt. Zudem wurden die technischen Komponenten für den Provisionerungsservice sowie die Authorisationsengine ([Univention Guardian](https://docs.software-univention.de/guardian-manual/latest/)) bereitgestellt.
- Zum vorgenannten Nubus-Stack gehört auch der Univention Keycloak, der mit den bereits bekannten [Univention Keycloak Extensions](https://gitlab.opencode.de/bmi/opendesk/component-code/crossfunctional/univention/ums-keycloak-extensions) vor Brute-Force-Angriffen schützt.
- Das Nextcloud Community Container-Image wurde ausgetauscht gegen skalierbare Container die stärker an den Anforderungen des IT-Grundschutzes ausgerichtet sind.
- openDesk spezifisches Linting wurde eingeführt, das sich zunächst um Konsistenz und Vollständigkeit der Image und Helm-Chart Definitionsdateien kümmert. Dadurch werden die Prozesse wie der [OCI-Mirror](https://gitlab.opencode.de/bmi/opendesk/tooling/oci-pull-mirror) oder Renovate abgesichert.
- Das [openDesk CI-Toolset "gitlab-config"](https://gitlab.opencode.de/bmi/opendesk/tooling/gitlab-config) wurde um eine Malware-Scan Stufe erweitert, welche u.a. auch bei Releases automatisiert einen Malwarescan über sämtliche Images durchführt.
- Ein Object Store wird als nicht produktive Servicekomponente bereitgestellt und aktuell bereits von Nextcloud, OpenProject und dem Univention Portal genutzt. Anwendungen wie Open-Xchange sollen folgen.
- [Intent-basierte Network-Policies](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-otterize) stehen zur optionalen Aktivierung bereit.

# Weitere Liefergegenstände

## Anwenderdokumentation

Die Anwenderdokumentation ist an zwei Stellen zu finden:

- [GitLab Pages](https://anwenderdokumentation-bmi-opendesk-documentation-9cd9537ae7c91f.usercontent.opencode.de/): Eine durchsuch- und navigierbare Darstellung der Anwenderdokumentationsinhalte.
- [Quell Repository](https://gitlab.opencode.de/bmi/opendesk/documentation/anwenderdokumentation): Hier sind die Ausgangsdateien der GitLab Pages zu finden.

## Architekturkonzept

Mit openDesk 24.03 gibt es keine Update des [openDesk Architekturkonzepts](https://gitlab.opencode.de/bmi/opendesk-architekturkonzept), die fehlenen Kapitel:

- [Phase F - Integrations-/ Migrationsplan](https://bmi.usercontent.opencode.de/opendesk-architekturkonzept/F_integrationsplan/)
- [Phase G - Governance Struktur](https://bmi.usercontent.opencode.de/opendesk-architekturkonzept/G_governance/)
- [Phase H - Architektur Change Management](https://bmi.usercontent.opencode.de/opendesk-architekturkonzept/H_changemanagement/)

befinden sich aktuell im Review des Architekturboards und werden im April 2024 nachgereicht.

## Barrierefreiheit

Die zum Release 24.03 aktualisierten Barrierefreiheitsberichte liegen für die folgenden Komponenten vor:

- [Admin Portal](./Barrierefreiheit/Barrierefreiheitsbericht%20-%20Komponente%20AdminPortal.pdf)
- [Diagramme](./Barrierefreiheit/Barrierefreiheitsbericht%20-%20Komponente%20Diagramme.pdf)
- [Direkte Kommunikation](./Barrierefreiheit/Barrierefreiheitsbericht%20-%20Komponente%20Direkte%20Kommunikation.pdf)
- [Kommunikation & Organisation](./Barrierefreiheit/Barrierefreiheitsbericht%20-%20Komponente%20Kommunikation%20&%20Organisation.pdf)
- [Portal](./Barrierefreiheit/Barrierefreiheitsbericht%20-%20Komponente%20Portal.pdf)
- [Projekte](./Barrierefreiheit/Barrierefreiheitsbericht%20-%20Komponente%20Projekte.pdf)
- [Sofort Videokonferenz](./Barrierefreiheit/Barrierefreiheitsbericht%20-%20Komponente%20Sofort%20Videokonferenz.pdf)
- [Weboffice](./Barrierefreiheit/Barrierefreiheitsbericht%20-%20Komponente%20Weboffice.pdf)
- [Wissen](./Barrierefreiheit/Barrierefreiheitsbericht%20-%20Komponente%20Wissen.pdf)

## Datenschutz

Die Datenschutzdokumentation des Releases 24.03 löst frühere Versionen ab und ist nun in einem eigenen Repository zu finden:

- [Repository Datenschutz](https://gitlab.opencode.de/bmi/opendesk/documentation/datenschutz)

## Erprobungskonzept

Eine Dokumentation zur Auswahl von Erprobungspartnern und zur Durchführung des Erprobungsbetriebs wird durch das Erprobungskonzept bereitgestellt.

- [Erprobungskonzept](https://gitlab.opencode.de/bmi/opendesk/documentation/erprobungskonzept)

## IT-Grundschutz

Die Prüfungsergebnisse hinsichtlich der IT-Grundschutzfähigkeit von openDesk werden gemäß der [*Hinweise zur Bereitstellung der Referenzdokumente im Rahmen
der Zertifizierung nach ISO 27001 auf der Basis von IT-Grundschutz*](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Grundschutz/Zertifikat/ISO27001/Hinweise_Referenzdokumente_Kompendium.pdf?__blob=publicationFile&v=1) zur Verfügung gestellt.

- [Repository IT-Grundschutzfähigkeit Prüfungsergebnisse](https://gitlab.opencode.de/bmi/opendesk/documentation/it-grundschutz)

## Lizenz-Compliance

Auf Basis der für den Releasestand erstellten SBOMs wurde ein Abweichungsreport erstellt. Dieser Bericht untersucht, ob und welche Lizenzabweichungen in Bezug auf die [Lizenzanforderungen von Open CoDE](https://wikijs.opencode.de/de/Hilfestellungen_und_Richtlinien/Lizenzcompliance) bestehen.

- [Abweichungsreport Lizenzen](https://gitlab.opencode.de/bmi/opendesk/deployment/SBOM/-/blob/main/sboms/openDesk%20Deviation%20Report-0.5.74.md)
- [Software Bill of Materials](https://gitlab.opencode.de/bmi/opendesk/deployment/SBOM/-/tree/main/sboms/0.5.74)

## Qualitätssicherung

Die Testergebnisse werden im Rahmen des Qualitatssicherungsreports in einer separaten Datei bereitgestellt. Diese, wie auch die darin verlinkten Unterberichte, sind englischsprachig verfasst.

- [QA-Report](./QA-REPORT.md)

## Releasekonzept

Das Releasekonzept beschreibt das Vorgehen zur Bereitstellung eines openDesk Releases, wie es für das Release 24.03 relevant war. Zukünftige Änderungen im Vorgehen sind durch aktuelle Umsetzungserkenntnisse möglich.

- [Konzept Releasemanagement](./Konzept%20Releasemanagement.pdf)

# Fußnoten

[^1]: Bei Nubus handelt es sich um das Cloud Portal und IAM von Univention. Aktuell ist ein Produkt Preview in openDesk integriert, weshalb die Dokumentation und Release-Notes noch nicht auf einem produktüblichen Stand sind. Der Source-Code ist [bereits auf Open CoDE zu finden](https://gitlab.opencode.de/bmi/opendesk/component-code/crossfunctional/univention). Neuigkeiten zu Nubus gibt es auf https://nubus.io.
